# Gamedil

you can play online at: gamedil.herokuapp.com  

to install: npm install  
to run locally: node app  
alternatively: nodemon app  
direct your browser to: localhost:3000  

Features:
- chat
- play rock/paper/scissors
- play a physicsbased 1v1 online game with projectiles that can explode parts of the map and bounce on it like a rubberball

to do:
- implement display of online players
- implement pseudo3d on client
- show background image
- show turntimer
- display names on players in game
- add turndelay
- display roundnumber, wind
- fix walking: currently able to climb too high sometimes
- optimize bulletCollisionPoints.timeToNextCheck (dont calculate time to hit players so often)
- hitpoints
- bombs? healthpackages?
- teleportation?


