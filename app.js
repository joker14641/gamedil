require('module-alias/register');

const { express, app, http, io, port } = require("./libs/server-constants");

const connectPlayers = require("./libs/connect-players");

app.use(express.static("public"));

io.on("connection", function (socket) {
    console.log("a user connected");
    connectPlayers.connect(socket);
});

http.listen(port, function () {
    console.log(`listening on *: ${port}`);
});
