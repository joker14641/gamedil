require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const fakePlayer = require("@test/fake").player;

const turnStub = sinon.stub();

const nextRound = proxyquire("@root/libs/gamedil/Game/next-round", {
    "./turn" :  turnStub
});


let game, players, player1, player2;
const freshStart = function () {
    sinon.resetHistory();
    players = [player1, player2] = [fakePlayer(), fakePlayer()];
    players.forEach((p) => {
        p.gamedil = {};
    });
    game = {
        players: players,
        round: 0,
        over: () => false
    };
}


describe("gamedil/next-round", function(){
    beforeEach(freshStart);
    describe("nextRound(game)", function(){
        it("it assigns turns to players in order of their delay and increases game.round by 1", async function(){
            player1.gamedil.delay = 11;
            player2.gamedil.delay = 42;
            await nextRound(game);
            assert(turnStub.firstCall.calledWith(game,player1));
            assert(turnStub.secondCall.calledWith(game,player2));
            expect(game.round).to.equal(1);

            freshStart();
            game.round = 5;
            player1.gamedil.delay = 11;
            player2.gamedil.delay = 2;
            await nextRound(game);
            assert(turnStub.firstCall.calledWith(game,player2));
            assert(turnStub.secondCall.calledWith(game,player1));
            expect(game.round).to.equal(6);

        });
    });
});
