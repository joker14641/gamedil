require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const fakePlayer = require("@test/fake").player;

const moveLoopStub = {
    start: sinon.stub(),
    stop: sinon.stub()
};
const createMoveLoopStub = sinon.stub().returns(moveLoopStub);

const groundedPromise = Promise.resolve();
const fallStub = {
    groundAllGamedilObjects: sinon.stub().returns(groundedPromise)
}

const turn = proxyquire("@root/libs/gamedil/Game/turn", {
    "./create-move-loop": createMoveLoopStub,
    "./fall": fallStub
});
const gamedilStartCharging = "gamedil-start-charging";
const gamedilStopCharging = "gamedil-stop-charging";
const gamedilAimangle = "gamedil-aimangle";

let clock, game, players, player;
const freshStart = function () {
    sinon.resetHistory();
    players = [fakePlayer(), fakePlayer()];
    players.forEach((p) => {
        p.gamedil = {
            aimAngle: 0
        };
    });
    player = players[0];
    game = {
        players: players,
        supportedWeapons: [],
        round: 0,
        over: () => false,
        broadcastTurnStart: sinon.stub(),
        broadcastTurnEnd: sinon.stub(),
        shoot: sinon.stub()
    };
}

describe("gamedil/turn", function () {
    beforeEach(function () {
        freshStart();
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    describe("turn(game,player)", function () {
        it("starts the moveLoop and stops it after 30 seconds", async function () {
            let turnPromise = turn(game, player);
            await groundedPromise;
            assert(fallStub.groundAllGamedilObjects.called, "should ground all gamedilObjects");
            assert(game.broadcastTurnStart.calledOnceWithExactly(player), "should tell player that his turn started");
            assert(!moveLoopStub.stop.called, "should not stop moveLoop before starting it");
            assert(createMoveLoopStub.calledOnceWithExactly(game, player), "should call createMoveLoop(game,player)");
            assert(moveLoopStub.start.calledOnceWithExactly(), "should start moveLoop");
            clock.tick(30000);
            assert(moveLoopStub.stop.calledOnceWithExactly(), "should stop moveLoop after 30 seconds");
            await turnPromise;
        });
        it("stops the moveLoop when player starts charging", async function () {
            let turnPromise = turn(game, player);
            await groundedPromise;
            player.socket.handleEvent(gamedilStartCharging);
            assert(moveLoopStub.stop.calledOnceWithExactly(), "should stop moving when player starts charging");
            await Promise.resolve(); //this is needed to make sure that the chargetimeout will be created before the clock is ticked
            clock.tick(3000);
            await turnPromise;
        });
        it("starts shooting 3 seconds after player started charging", async function () {
            let turnPromise = turn(game, player);
            await groundedPromise;
            player.socket.handleEvent(gamedilStartCharging);
            await Promise.resolve();
            clock.tick(3000);
            await turnPromise;
            assert(game.shoot.calledOnce, "should have shot");
            assert(game.shoot.args[0][0] === player, "player should have shot");
            assert(game.shoot.args[0][1] === 1, "should shoot with full power");
        });
        it("shoots with less than full power if player stops charging early", async function () {
            let turnPromise = turn(game, player);
            await groundedPromise;
            player.socket.handleEvent(gamedilStartCharging);
            await Promise.resolve();
            clock.tick(1000);
            player.socket.handleEvent(gamedilStopCharging);
            await turnPromise;
            assert(game.shoot.calledOnce, "should have shot");
            assert(game.shoot.args[0][0] === player, "player should have shot");
            assert(game.shoot.args[0][1] > 0.3333 && game.shoot.args[0][1] < 0.3334, "should shoot with about 1/3 power");
        });
        it("while charging player can change where he is aiming", async function () {
            let turnPromise = turn(game, player);
            await groundedPromise;
            player.socket.handleEvent(gamedilStartCharging);
            await Promise.resolve();
            const angle = 2 * Math.PI * Math.random();
            player.socket.handleEvent(gamedilAimangle, angle);
            clock.tick(3000);
            await turnPromise;
            assert(player.gamedil.aimAngle === angle, "should let player set aimangle");
        });
        it("player can't set invalid aimangles", async function(){
            let turnPromise = turn(game, player);
            await groundedPromise;
            player.socket.handleEvent(gamedilStartCharging);
            await Promise.resolve();
            const angle = "Elephant";
            player.socket.handleEvent(gamedilAimangle, angle);
            clock.tick(3000);
            await turnPromise;
            assert(player.gamedil.aimAngle === 0, "should validate aimAngle");
        });
    });
});