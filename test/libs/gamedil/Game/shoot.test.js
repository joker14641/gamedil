require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const fakePlayer = require("@test/fake").player;

const firePromise = Promise.resolve();
const bulletStub ={
    fire: sinon.stub().returns(firePromise)
};
const createBulletStub = sinon.stub().returns(bulletStub);

const shoot = proxyquire("@root/libs/gamedil/Game/shoot", {
    "./shoot/create-bullet": createBulletStub
});

describe("gamedil/Game/shoot", function () {
    afterEach(sinon.resetHistory);
    describe("shoot(game,player,power)", function () {
        it("creates a bullet and fires it", async function () {
            const game = {
                objects: [],
                map: {}
            };
            const player = {};
            const power = Math.random();
            shoot(game, player, power);
            assert(createBulletStub.calledOnceWith(game, player, power),
                "should create a bullet for player");
            assert(bulletStub.fire.calledOnceWithExactly(), "should fire bullet");
            await firePromise;
        });
    });
});