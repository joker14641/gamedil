require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const createBulletCollisionPoint = require("@root/libs/gamedil/Game/shoot/create-bullet-collision-point");

describe("gamedil/Game/shoot/create-bullet-collison-point", function () {
    let bullet, collisionPointLocation, map;
    beforeEach(() => {
        map = {
            collisionsWith: sinon.stub(),
            timeWithNoCollisions: sinon.stub()
        };
        bullet = {
            canCollideWith: [map],
            motionData: {
                position: {
                    x: 0,
                    y: 0
                },
                velocity: {
                    vx: 0,
                    vy: 0
                }
            },
            registerCollisions: sinon.stub(),
            getFutureMotionData: sinon.stub()
        };
        collisionPointLocation = {
            position: {
                x: 0,
                y: 0
            },
            normal: {
                nx: 1,
                ny: 0
            }
        };
    });
    afterEach(sinon.resetHistory);
    describe("createBulletCollisionPoint(bullet, collisionPointLocation)", function () {
        it("creates a BulletCollisionPoint", function () {
            const bcp = createBulletCollisionPoint(bullet, collisionPointLocation);
            expect(bcp.timeToNextCheck).to.be.a("number",
                "should have timeToNextCheck property, that has time in ms");
            expect(bcp.checkForCollisions).to.be.a("Function",
                "should have checkForCollisions method");
        });
    });
    describe("BulletCollisionPoint.checkForCollisions()", function () {
        it("if there is no collision it updates timeToNextCheck and returns false", function () {
            map.collisionsWith.returns([]);

            const bcp = createBulletCollisionPoint(bullet, collisionPointLocation);
            sinon.stub(bcp, "_prepareNextCheck");

            const returnValue = bcp.checkForCollisions();

            expect(returnValue).to.equal(false, "should return false if no collision occurs");
            assert(bcp._prepareNextCheck.calledOnce, "should update time to next check");
            assert(map.collisionsWith.calledWith(bcp.motionData.position),
                "should check for collision with map at current position");

            map.collisionsWith.reset();
            bcp._prepareNextCheck.restore();
        });
        it("if there are collisions it returns true and registers the collisions", function () {
            const collisions = [{}];
            map.collisionsWith.returns(collisions);

            const bcp = createBulletCollisionPoint(bullet, collisionPointLocation);
            sinon.stub(bcp, "_prepareNextCheck");

            const returnValue = bcp.checkForCollisions();

            expect(returnValue).to.equal(true, "should return true if collision occurs");
            assert(map.collisionsWith.calledWith(bcp.motionData.position),
                "should check for collision with map at current position");
            assert(bullet.registerCollisions.calledWith(collisions));
            map.collisionsWith.reset();
            bcp._prepareNextCheck.restore();
        });
    });
    describe("BulletCollisionPoint._prepareNextCheck()", function () {
        it("updates current velocity and position and determines when to check for collision again", function () {
            const bcp = createBulletCollisionPoint(bullet, collisionPointLocation);
            for (let i = 0; i < 10; i++) {
                const expectedTimeToNextCheck = Math.random();
                map.timeWithNoCollisions.returns(expectedTimeToNextCheck);
                const expectedMotionData = {};
                bullet.getFutureMotionData.returns(expectedMotionData);

                const oldMotionData = bcp.motionData;

                bcp._prepareNextCheck();

                const actualMotionData = bcp.motionData;

                expect(actualMotionData).to.equal(expectedMotionData,
                    "should set next motionData to future data");
                assert(bullet.getFutureMotionData.calledOnceWithExactly(oldMotionData, expectedTimeToNextCheck),
                    "should calculate future position/velocity");
                expect(bcp.timeToNextCheck).to.equal(expectedTimeToNextCheck, 
                    "should update timeToNextCheck");

                map.timeWithNoCollisions.reset();
                bullet.getFutureMotionData.reset();
            }

        });
    });
});