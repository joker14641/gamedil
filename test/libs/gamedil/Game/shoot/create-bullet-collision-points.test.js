require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const minTimePlanned = 1000;
let collisionProbability = 0;
class PseudoBulletCollisionPoint {
    constructor(location) {
        const checkPoints = [];
        let t = 0;
        class CheckPoint {
            constructor(timeStamp) {
                this.timeStamp = timeStamp;
                this.checked = false;
                this.collides = Math.random() < collisionProbability;
            }
        }
        while (t < minTimePlanned) {
            t += Math.random() * 100;
            let checkPoint = new CheckPoint(t);
            checkPoints.push(checkPoint);

        }
        checkPoints.push(new CheckPoint(Infinity));
        this.testInfo = { checkPoints, updates: 0 };
        this.timeToNextCheck = checkPoints[0].timeStamp;
    }
    checkForCollisions() {
        const checkPoints = this.testInfo.checkPoints;
        const i = this.testInfo.updates;
        this.timeToNextCheck = checkPoints[i + 1].timeStamp - checkPoints[i].timeStamp;
        checkPoints[i].checked = true;
        this.testInfo.updates++;
        return checkPoints[i].collides;
    }
}

const createBulletCollisionPointSpy = sinon.spy(loc => new PseudoBulletCollisionPoint(loc));

const createBulletCollisionPoints = proxyquire("@root/libs/gamedil/Game/shoot/create-bullet-collision-points", {
    "./create-bullet-collision-point": createBulletCollisionPointSpy
});

describe("gamedil/Game/shoot/create-bullet-collison-points", function () {
    let bullet, collisionPointsPattern;
    beforeEach(() => {
        bullet = {};
        collisionPointsPattern = Array(16).fill(0).map(x => ({}));
    });
    afterEach(sinon.resetHistory);
    describe("createBulletCollisionPoints(bullet, collisionPointPattern)", function () {
        it("creates collisionpoints for the bullet", function () {
            const bulletCollisionPoints = createBulletCollisionPoints(bullet, collisionPointsPattern);
            for (const loc of collisionPointsPattern) {
                assert(createBulletCollisionPointSpy.calledWith(bullet, loc),
                    "should create collisionPoint for every location in the pattern");
            }
            expect(bulletCollisionPoints.update).to.be.a("function", "should have update method");
        });
    });
    describe("BulletCollisionPoints.update(t)", function () {
        it("if no collision will happen, it updates all collisionpoints that need to be updated", function () {
            collisionProbability = 0;
            const bulletCollisionPoints = createBulletCollisionPoints(bullet, collisionPointsPattern);
            const t = 1000;
            const returnValue = bulletCollisionPoints.update(t);
            for (const bcp of createBulletCollisionPointSpy.returnValues) {
                for (const checkPoint of bcp.testInfo.checkPoints) {
                    if (checkPoint.timeStamp < t) {
                        assert(checkPoint.checked, "didn't check a collisionPoint often enough");
                    }
                    else {
                        assert(!checkPoint.checked, "checked a collisionPoint too often");
                    }
                }
            }
            expect(returnValue).to.equal(0, "should return 0 if no collisions happened");
        });
        it("it stops updating after the first collision happens", function () {
            collisionProbability = 0.1;
            count = 0;
            const bulletCollisionPoints = createBulletCollisionPoints(bullet, collisionPointsPattern);
            const t = 1000;
            const returnValue = bulletCollisionPoints.update(t);
            const firstCollision = Math.min(...createBulletCollisionPointSpy.returnValues
                .map(bcp => bcp.testInfo.checkPoints
                    .filter(checkPoint => checkPoint.collides))
                .map(arr => arr.length > 0 ? arr[0].timeStamp : Infinity));
            if (firstCollision < t) {
                assert.closeTo(returnValue, t - firstCollision, 0.001);
                for (const bcp of createBulletCollisionPointSpy.returnValues) {
                    for (const checkPoint of bcp.testInfo.checkPoints) {
                        if (checkPoint.timeStamp <= firstCollision) {
                            assert(checkPoint.checked, "didn't check a collisionPoint often enough");
                        }
                        else {
                            assert(!checkPoint.checked, "checked a collisionPoint after first collision");
                        }
                    }
                }

            }
        });
    });
});