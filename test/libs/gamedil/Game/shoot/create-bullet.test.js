require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const fakePlayer = require("@test/fake").player;

const bulletCollisionPointsStub = {
    update: (t) => {
        let r = Math.random();
        let s = Math.random()*t;
        return r > 0.1? 0 : s;
    }
};
const createBulletCollisionPointsStub = sinon.stub().returns(bulletCollisionPointsStub);
const createBullet = proxyquire("@root/libs/gamedil/Game/shoot/create-bullet", {
    "./create-bullet-collision-points": createBulletCollisionPointsStub
});

describe("gamedil/Game/shoot/create-bullet", function () {
    let game, player, power, canCollideWith;
    beforeEach(() => {
        game = {
            map: {
                gravity: 0,
                wind: 0
            },
            supportedWeapons: ["cannon"],
            bullets: new Map([
                ["cannon", {
                    collisionPointsPattern: {}
                }]
            ]),
            bulletsInAir: [],
            broadcastState: sinon.stub()
        };
        player = fakePlayer();
        player.gamedil = {
            weapon: "cannon",
            position: { x: 0, y: 0 }
        };
        power = Math.random();
        canCollideWith = [];
    });
    afterEach(sinon.resetHistory);
    describe("createBullet(game,player, power, canCollideWith)", function () {
        it("creates a bullet", function () {
            const cannonCollisionPointsPattern = game.bullets.get("cannon").collisionPointsPattern;
            const bullet = createBullet(game, player, power, canCollideWith);
            expect(bullet.fire).to.be.a("Function", "bullet should have method to fire it");
            expect(bullet.type).to.equal(player.gamedil.weapon, "bullet.type should equal player's weapon");
            assert(createBulletCollisionPointsStub.calledOnceWithExactly(bullet, cannonCollisionPointsPattern),
                "should create collisionpoints from pattern");
            expect(bullet.collisionPoints).to.equal(bulletCollisionPointsStub, "bullet should have BulletCollisionPoints");
            expect(bullet.registerCollisions).to.be.a("Function",
                 "should be able to register collisions");
        });
    });
    describe("Bullet.fire()", function () {
        it("advances the bullet until the shot is complete", async function () {
            const clock = sinon.useFakeTimers();
            sinon.spy(bulletCollisionPointsStub, "update");
            // bulletCollisionPointsStub.update.returns(0);
            // for(let i= 0; i< 100; i++){
                
            //     bulletCollisionPointsStub.update.onCall(i).returns();
            // }            
            const bullet = createBullet(game, player, power, canCollideWith);
            const firePromise = bullet.fire();
            const shotDuration = 2000*Math.random();
            setTimeout(() => { bullet.shotIsComplete = true; }, shotDuration);
            while (Date.now() < shotDuration) {
                await Promise.resolve();
                clock.next();
                if(Math.random() > 0.5){
                    bullet.registerCollisions([{}]);
                }
            }
            clock.runAll();
            await firePromise;
            const sumArgs = bulletCollisionPointsStub.update.args.reduce((prev, cur) => prev + cur[0], 0);
            const sumReturns = bulletCollisionPointsStub.update.returnValues.reduce((prev, cur) => prev + cur, 0);
            const totalUpdateTime = sumArgs -sumReturns;
            assert(totalUpdateTime >= shotDuration, "total update time is too small");
            assert(totalUpdateTime < shotDuration+1000/60, "total update time is too long");
            assert(bulletCollisionPointsStub.update.callCount > shotDuration / 1000 * 50, "should update collisionPoints frequently");
            clock.restore();
            bulletCollisionPointsStub.update.restore();
        });
    });
});