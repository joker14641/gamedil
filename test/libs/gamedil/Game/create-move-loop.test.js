require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const fakePlayer = require("@test/fake").player;

const canMoveStub = {
    left: sinon.stub(),
    right: sinon.stub(),
    up: sinon.stub(),
    down: sinon.stub()
};
const gamedilMoveInDirection = "gamedil-move-in-direction";
const [left, stand, right] = [-1, 0, 1];
const directions = [left, stand, right];

const createMoveLoop = proxyquire("@root/libs/gamedil/Game/create-move-loop", {
    "./can-move": canMoveStub
});

const mapStub = {
    isAir: sinon.stub(),
    isOutOfBoundary: sinon.stub().returns(false)
};

let game, player, clock;

describe("gamedil/create-move-loop", function () {
    beforeEach(function () {
        player = fakePlayer();
        player.gamedil = {
            position: { x: 42, y: 11 },
            collisionPoints: [{ x: 0, y: 0 }]
        };
        game = {
            map: mapStub,
            broadcastState: sinon.stub()
        };
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
        sinon.resetHistory();
        canMoveStub.down.reset();
    });
    describe("createMoveLoop(game, player)", function () {
        it("has start and stop methods", function () {
            const moveLoop = createMoveLoop(game, player);
            expect(moveLoop.start).to.be.a("function");
            expect(moveLoop.stop).to.be.a("function");
        });
        // it("makes the player fall straight down onto solid ground on the map", function () {
        //     const testFallDistance = 100;
        //     for (let i = 0; i < testFallDistance; i++) {
        //         canMoveStub.down.onCall(i).returns(true);
        //     }
        //     canMoveStub.down.returns(false);
        //     const oldY = player.gamedil.position.y;
        //     const oldX = player.gamedil.position.x;
        //     const moveLoop = createMoveLoop(game, player);
        //     moveLoop.start();
        //     clock.tick(1000);
        //     moveLoop.stop();
        //     assert(canMoveStub.down.called, "called canmove");
        //     expect(player.gamedil.position.y).to.equal(oldY + testFallDistance,
        //         "y should increase by testFallDistance");
        //     expect(player.gamedil.position.x).to.equal(oldX,
        //         "x shouldn't change when falling");
        // });
        it("doesn't change player's position if he is standing on solid ground", function () {
            canMoveStub.down.returns(false);
            const oldY = player.gamedil.position.y;
            const oldX = player.gamedil.position.x;
            const moveLoop = createMoveLoop(game, player);
            moveLoop.start();
            clock.tick(1000);
            moveLoop.stop();
            expect(player.gamedil.position.y).to.equal(oldY,
                "y should stay the same");
            expect(player.gamedil.position.x).to.equal(oldX,
                "x should stay the same");
        });
        it("regularly broadcasts the state of the game", function () {
            const moveLoop = createMoveLoop(game, player);
            moveLoop.start();
            clock.tick(1000);
            moveLoop.stop();
            assert(game.broadcastState.callCount >30);
        });
    });
});