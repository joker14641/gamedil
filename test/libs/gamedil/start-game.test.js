require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const gamedilLoaded = "gamedil-loaded";
const gamedilFailedToLoad = "gamedil-failed-to-load";
const gamedilAssignOwnMatchId = "gamedil-assign-own-match-id";

// const ioStub = {
//     "to": sinon.stub()
// };
// const ioToRoomStub = {
//     "emit": sinon.stub()
// };
// ioStub.to.returns(ioToRoomStub);

// const loadMapStub = sinon.stub();

const chatStub = sinon.stub();
chatStub.sendMessage = sinon.stub();
const testRoom = "testroom";
chatStub.createRoom = sinon.stub().returns(testRoom);

const createGameStub = sinon.stub();
const gameStub = {
    load: sinon.stub(),
    start: sinon.stub(),
    broadcast: sinon.stub()
};
createGameStub.returns(gameStub);

const startGame = proxyquire("@root/libs/gamedil/start-game", {
    // "@root/models/server-constants": { io: ioStub },
    "../chat": chatStub,
    // "./load-map": loadMapStub,
    "./Game/create-game": createGameStub
});

const fakePlayer = require("@root/test/fake").player;

let players, player1, player2, startPromise;
const freshStart = function () {
    sinon.resetHistory();
    players = [player1, player2] = [fakePlayer(), fakePlayer()];
    players.forEach((p) => {
        p.gamedil = { position: { set: sinon.stub() } };
    });
}

describe("gamedil/start-game", function () {
    beforeEach(freshStart);
    describe("startGame(player1,player2)", function () {
        it("calls createGame with [player1, player2] as first argument and attempts to load the game", function () {
            startGame(...players);
            assert(createGameStub.calledOnce, "should create exactly one game");
            expect(createGameStub.args[0][0]).to.have.members(players);
            assert(gameStub.load.calledOnceWithExactly(), "should call game.load() exactly once");
        });
        it("if game loads successfully, it starts the game", async function(){
            gameStub.load.returns(Promise.resolve());
            await startGame(...players);
            assert(gameStub.start.calledOnceWithExactly(), "should call game.start() exactly once");
            gameStub.load.resetBehavior();
        });
        it("it can handle load failure", async function(){
            gameStub.load.returns(Promise.reject());

            players.forEach(p => {p.gamedil.inGame = true;});
            await startGame(...players);
            assert(gameStub.start.notCalled, "should not call game.start()");
            assert(gameStub.broadcast.calledOnceWithExactly(gamedilFailedToLoad), "should use game.broadcast to broadcast the load failure");
            players.forEach(p => {
                assert(!p.gamedil.inGame, "players should not be in game");
            });

            gameStub.load.resetBehavior();
        });
    });
});

// describe("gamedil/start-game", function () {
//     beforeEach(freshStart);
//     describe("startGame(player1,player2)", function () {
//         it("calls chat.createRoom(player1,player2)", function () {
//             startPromise = startGame(...players);
//             sinon.assert.calledWith(chatStub.createRoom, "Gamedil", player1, player2);
//         });
//         it("puts both players inGame", function () {
//             startPromise = startGame(...players);
//             players.forEach(function (p) {
//                 assert(p.gamedil.inGame);
//             });
//         });
//         it("it uses io.to(room) to emit load-gamedil event with correct data to load", function () {
//             startPromise = startGame(...players);
//             sinon.assert.calledWith(ioStub.to, testRoom);
//             expect(ioToRoomStub.emit.firstCall.args[0]).to.equal("load-gamedil");
//             expect(ioToRoomStub.emit.firstCall.args[1]).to.have.property("map");
//             expect(ioToRoomStub.emit.firstCall.args[1].map).to.equal("map4.png");
//         });
//         it("if both players and the map loads, it emits gamedil-loaded to the room, doesnt emit gamedil-failed-to-load, emits to each player their id and it calls game.start()", async function () {
//             loadMapStub.returns(new Promise((resolve) => { resolve("bla"); }));
//             startPromise = startGame(...players);
//             players.forEach(function (p) {
//                 p.socket.handleEvent(gamedilLoaded);
//             });
//             players.forEach(p => {
//                 p.gamedil.id = Math.random()
//             });
//             await startPromise;
//             assert(ioToRoomStub.emit.calledWith(gamedilLoaded), "should emit gamedilLoaded");
//             assert(ioToRoomStub.emit.neverCalledWith(gamedilFailedToLoad), "should not emit gamedilFailedToLoad");
//             loadMapStub.reset();
//             players.forEach(p => {
//                 assert(p.socket.emit.calledWith(gamedilAssignOwnMatchId, p.gamedil.id), "should announce correct ids to each player")
//             });
//             assert(gameStub.start.calledWith(), "should call game.start()");
//         });
//         it("if players or map fails to load, it emits gamedil-failed-to-load and doesn't emit gamedil-loaded", async function () {
//             const doTest = async () => {
//                 await startPromise;
//                 assert(ioToRoomStub.emit.calledWith(gamedilFailedToLoad));
//                 assert(ioToRoomStub.emit.neverCalledWith(gamedilLoaded));
//             }
//             startPromise = startGame(...players);
//             player1.socket.handleEvent(gamedilFailedToLoad);
//             await doTest();

//             freshStart();
//             startPromise = startGame(...players);
//             player2.socket.handleEvent(gamedilFailedToLoad);
//             await doTest();

//             freshStart();
//             loadMapStub.returns(new Promise((resolve, reject) => { reject(); }));
//             startPromise = startGame(...players);
//             await doTest();
//             loadMapStub.reset();
//         });
//         it("a player can only load or fail to load once", function () {
//             /**
//              * does the test with player[i]
//              * @param {number} i 
//              */
//             const doTests = (i) => {
//                 //simulate successful load
//                 freshStart();
//                 let player = players[i];
//                 startGame(...players);
//                 player.socket.handleEvent(gamedilLoaded);
//                 sinon.assert.calledWith(player.socket.off, gamedilLoaded, player.gamedil.onLoad);
//                 sinon.assert.calledWith(player.socket.off, gamedilFailedToLoad, player.gamedil.onLoadError);

//                 //simulate failed load
//                 freshStart();
//                 player = players[i];
//                 startGame(...players);
//                 player.socket.handleEvent(gamedilFailedToLoad);
//                 sinon.assert.calledWith(player.socket.off, gamedilLoaded, player.gamedil.onLoad);
//                 sinon.assert.calledWith(player.socket.off, gamedilFailedToLoad, player.gamedil.onLoadError);
//             }
//             doTests(0);
//             doTests(1);
//         });
//     });
// });
