require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const updateMap = require("@root/libs/gamedil/update-map");
const updateMapStub = sinon.stub();
const loadMap = proxyquire("@root/libs/gamedil/load-map", {
    "./update-map": updateMapStub
});
const mapSrc = "map4.png";

const solid = 2;
const mixed = 1;
const air = 0;
let map;
describe("gamedil/update-map (Relies on load-map to work properly)", function () {
    before(async () => {
        map = await loadMap(mapSrc);
    });
    describe("update(map,x,y,w,h)", function () {
        it("updates the map correctly", function () {
            this.timeout(10000);
            map.ctx.fillStyle = Math.random() < 0.5 ? "#FF0000" : "#FFFFFF";
            const dx = map.width * Math.random();
            const dy = map.height * Math.random();
            const dw = map.width * Math.random();
            const dh = map.height * Math.random();
            map.ctx.fillRect(dx, dy, dw, dh);
            updateMap(map, dx, dy, dw, dh);

            // var fs = require('fs')
            //     , out = fs.createWriteStream(__dirname + '/text.png')
            //     , stream = map.canvas.pngStream();

            // stream.on('data', function (chunk) {
            //     out.write(chunk);
            // });

            // stream.on('end', function () {
            //     console.log('saved png');
            // });

            const width = map.width;
            const height = map.height;
            const layer0 = map.layer[0];
            const imgData = map.ctx.getImageData(0, 0, width, height).data
            for (let y = 0, i = 0; y < height; y++) {
                for (let x = 0; x < width; x++ , i += 4) {
                    const mapDatum = layer0[x][y];
                    if (mapDatum.size !== 1) {
                        assert.fail("map.layer[0] should contain size 1 data");
                    }
                    if (mapDatum.x !== x) {
                        assert.fail("x coordinate is wrong");
                    }
                    if (mapDatum.y !== y) {
                        assert.fail("y coordinate is wrong");
                    }
                    if (imgData[i + 3] < 255 || //pixel is transparent
                        (imgData[i] === 255 && imgData[i + 1] === 255 && imgData[i + 2] === 255) //pixel is white
                    ) {
                        assert(mapDatum.data === air, "transparent or white pixels should be air");
                    }
                    else {
                        assert(mapDatum.data === solid, "non-transparent, non-white pixels should be solid");
                    }
                }
            }
            for (let depth = 1, size = 2; size <= Math.min(width, height); depth++ , size *= 2) {
                expect(map.layer, "should have more layers").to.have.property(depth);
                const layer = map.layer[depth];
                for (let y = 0; y < height; y++) {
                    for (let x = 0; x < width; x++) {
                        const mapDatum = layer[x][y];
                        if (mapDatum.size !== size) {
                            assert.fail("wrong size");
                        }
                        if (Math.abs(mapDatum.x - x) > size) {
                            assert.fail("wrong x");
                        }
                        if (Math.abs(mapDatum.y - y) > size) {
                            assert.fail("wrong y");
                        }
                        switch (mapDatum.data) {
                            case air:
                                if (map.layer[depth - 1][x][y].data !== air) {
                                    assert.fail("if area is air on one layer, it should be air on the previous layer");
                                }
                                break;
                            case solid:
                                if (map.layer[depth - 1][x][y].data !== solid) {
                                    assert.fail("if area is solid on one layer, it should be solid on the previous layer");
                                }
                                break;
                            default:
                                if (mapDatum.data !== mixed) {
                                    assert.fail("there should only be air, solid and mixed areas");
                                }
                                break;
                        }
                        if (map.layer[depth - 1][x][y].data === mixed) {
                            if (mapDatum.data != mixed) {
                                assert.fail("if area is mixed on previous layer, it should be mixed on next layer");
                            }
                        }
                    }
                }
            }
        });
    });
});