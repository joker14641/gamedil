require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const { createGamedilObject, createGamedilPlayer } = require("@root/libs/gamedil/Gamedil-object");

const src = "testsrc" + Math.random();
describe("gamedil/Gamedil-object", function () {
    describe("createGamedilObject(src)", function () {
        it("creates a GamedilObject", function () {
            const gamedil = createGamedilObject(src);
            expect(gamedil).to.have.property("state");
            expect(gamedil.state.id).to.equal(gamedil.id);
            expect(gamedil.state.position).to.equal(gamedil.position);
            expect(gamedil).to.have.property("loadData");
            expect(gamedil.loadData.id).to.equal(gamedil.id);
            expect(gamedil.loadData.src).to.equal(gamedil.src);
            expect(gamedil).to.have.property("position");
            assert(gamedil.src === src);
            expect(gamedil).to.have.property("position");
            expect(gamedil.position).to.have.property("x");
            expect(gamedil.position).to.have.property("y");
            expect(gamedil.inGame).to.be.false;
        });
    });
    describe("createGamedilPlayer(src)", function () {
        it("creates a GamedilPlayer", function () {
            const gamedil = createGamedilPlayer(src);
            expect(gamedil).to.have.property("state");
            expect(gamedil.state.id).to.equal(gamedil.id);
            expect(gamedil.state.position).to.equal(gamedil.position);
            expect(gamedil).to.have.property("loadData");
            expect(gamedil.loadData.id).to.equal(gamedil.id);
            expect(gamedil.loadData.src).to.equal(gamedil.src);
            expect(gamedil).to.have.property("position");
            expect(gamedil.src).to.equal(src);
            expect(gamedil).to.have.property("position");
            expect(gamedil.position).to.have.property("x");
            expect(gamedil.position).to.have.property("y");
            expect(gamedil.inGame).to.be.false;
        });
    });
});