require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const { createCanvas, loadImage } = require("canvas");
const updateMapStub = sinon.stub();
const loadMap = proxyquire("@root/libs/gamedil/load-map", {
    "./update-map": updateMapStub
});
const solid = 2;
const mixed = 1;
const air = 0;

const mapSrces = ["map4.png", "map.png", "map2.png", "map3.png", "map5.png","map.svg"];//.map((src) => "public/graphics/"+src); 
const mapImagesPromises = Promise.all(mapSrces.map(mapSrc => loadImage("public/graphics/"+mapSrc)));
let mapImages, imgData =[];

describe("gamedil/load-map", function () {
    before(async function () {
        mapImages = await mapImagesPromises;
        mapImages.forEach((img) => {
            const canvas = createCanvas(img.width, img.height);
            const ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            imgData.push( ctx.getImageData(0, 0, img.width, img.height).data);
        });
    });
    describe("loadMap(src)", function () {
        it("throws an error, if src is invalid", async function () {
            try {
                await loadMap("invalid src");
                assert.fail("shouldn't be able to load");
            } catch (error) { }
        });

        // it("load speed test", async function(){
        //     const maps = await Promise.all(mapImages.map((img) => loadMap(img.src)));
        // });
        
        //this test is slow. I used conditional assert.fail()s instead of assert(condition) to speed it up significantly (50sec -> 3 secs). expect(...) is even slower.
        it("loaded map is correctly initialized (on my pc this test takes about 3 seconds)", async function () {
            this.timeout(10000);
            const maps = await Promise.all(mapSrces.map(loadMap));
            mapImages.forEach ((img, index)  =>{
                const map = maps[index];
                sinon.resetHistory();
                map.update("x", "y","w", "h");
                assert(updateMapStub.calledOnceWithExactly(map,"x", "y","w", "h"), ".update() should call update-map with correct arguments");
                const width = img.width;
                const height = img.height;
                expect(map.width, "map.width").to.equal(width);
                expect(map.height, "map.height").to.equal(height);
                const layer0 = map.layer[0];
                for (let y = 0, i=0; y < height; y++) {
                    for (let x = 0; x < width; x++, i+=4) {
                        const mapDatum = layer0[x][y];
                        if(mapDatum.size !==1){
                            assert.fail("map.layer[0] should contain size 1 data");
                        }
                        if(mapDatum.x !==x){
                            assert.fail("x coordinate is wrong");
                        }
                        if(mapDatum.y !==y){
                            assert.fail("y coordinate is wrong");
                        }
                        // assert(mapDatum.size ===1, "map.layer[0] should contain size 1 data");
                        // assert(mapDatum.x ===x, "x coordinate is wrong, x:" + x + " y:"+y + " actual x:"+ mapDatum.x + "actual y:"+mapDatum.y);
                        // assert(mapDatum.y ===y, "y coordinate is wrong");
                        if (imgData[index][i + 3] < 255 || //pixel is transparent
                            (imgData[index][i] === 255 && imgData[index][i + 1] === 255 && imgData[index][i + 2] === 255) //pixel is white
                        ) {
                            //the test is considerably faster when using assert instead of expect
                            // expect(mapDatum.data, "transparent or white pixels should be air").to.equal(air);
                            assert(mapDatum.data === air, "transparent or white pixels should be air");
                        }
                        else {
                            //the test is considerably faster when using assert
                            // expect(mapDatum.data, "non-transparent, non-white pixels should be solid").to.equal(solid);
                            assert(mapDatum.data === solid, "non-transparent, non-white pixels should be solid");
                        }
                    }
                }
                for(let depth = 1, size = 2; size <= Math.min(width, height); depth++, size*=2){
                    expect(map.layer, "should compute more layers").to.have.property(depth);
                    const layer = map.layer[depth];
                    for(let y = 0; y < height; y++){
                        for(let x=0; x<width; x++){
                            const mapDatum = layer[x][y];
                            if(mapDatum.size !== size){
                                assert.fail("wrong size");
                            }
                            // assert(mapDatum.size === size, "wrong size");
                            if(Math.abs(mapDatum.x -x) > size){
                                assert.fail("wrong x");
                            }
                            // assert(Math.abs(mapDatum.x -x) <= size, "wrong x");
                            if(Math.abs(mapDatum.y - y) > size){
                                assert.fail("wrong y");
                            }
                            // assert(Math.abs(mapDatum.y - y) <= size, "wrong y");
                            switch (mapDatum.data) {
                                case air:
                                    if(map.layer[depth-1][x][y].data !== air){
                                        assert.fail("if area is air on one layer, it should be air on the previous layer");
                                    }
                                    // assert(map.layer[depth-1][x][y].data === air, "if area is air on one layer, it should be air on the previous layer");
                                    break;
                                case solid:
                                    if(map.layer[depth-1][x][y].data !== solid){
                                        assert.fail("if area is solid on one layer, it should be solid on the previous layer");
                                    }
                                    // assert(map.layer[depth-1][x][y].data === solid, "if area is solid on one layer, it should be solid on the previous layer");
                                    break;
                                default:
                                    if(mapDatum.data !== mixed){
                                        assert.fail("there should only be air, solid and mixed areas");
                                    }
                                    // assert(mapDatum.data === mixed, "there should only be air, solid and mixed areas");
                                    break;
                            }
                            if(map.layer[depth-1][x][y].data === mixed){
                                if(mapDatum.data != mixed){
                                    assert.fail("if area is mixed on previous layer, it should be mixed on next layer");
                                }
                                // assert(mapDatum.data === mixed, "if area is mixed on previous layer, it should be mixed on next layer");
                            }
                        }
                    }
                }
                
            })
        });
    });
});