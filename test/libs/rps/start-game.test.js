require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const chatStub = sinon.stub();
chatStub.sendMessage = sinon.stub();
const testRoom = "testroom";
chatStub.createRoom = sinon.stub().returns(testRoom);

const startGame = proxyquire("@root/libs/rps/start-game", {
    "../chat": chatStub
});
const fakePlayer = require("@test/fake").player;
const rps = "RPS";

let players, player1, player2;
const freshStart = function () {
    sinon.resetHistory();
    players = [player1, player2] = [fakePlayer(), fakePlayer()];
    players.forEach((p) => {
        p.rps = {};
    });
    startGame(...players);
}
beforeEach(freshStart);
describe("rps/start-game", function () {
    describe("startGame(player1, player2)", function () {
        it("puts both players into an RPS game", function () {
            players.forEach((p) => {
                assert(p.rps.inGame, "player should be in game");
            });
        });
        it("puts both players into a chatroom with purpose RPS", function () {
            sinon.assert.calledWith(chatStub.createRoom, rps, ...players);
        });
        it("sends a chatmessage to the room", function () {
            expect(chatStub.sendMessage.firstCall.args[0], "first argument of first sendMessage call ")
                .to.equal(testRoom);
        });
        it("after both players picked R,P or S, the game ends", function () {
            const choices = ["R", "P", "S"];
            for (let choice1 of choices) {
                for (let choice2 of choices) {
                    freshStart();
                    //player1 picks first
                    player1.socket.handleEvent(rps, choice1);
                    player2.socket.handleEvent(rps, choice2);
                    assert(!player1.rps.inGame, "player1 shouldn't be inGame");
                    assert(!player2.rps.inGame, "player2 shouldn't be inGame");

                    freshStart();
                    //player2 picks first
                    player2.socket.handleEvent(rps, choice1);
                    player1.socket.handleEvent(rps, choice2);
                    assert(!player1.rps.inGame, "player1 shouldn't be inGame");
                    assert(!player2.rps.inGame, "player2 shouldn't be inGame");
                }
            }
        });
        it("game shouldn't end if someone made an illegal choice", function () {
            const simulate = (choice1, choice2) => {
                freshStart();
                //player1 picks first
                player1.socket.handleEvent(rps, choice1);
                player2.socket.handleEvent(rps, choice2);
                players.forEach((p) => {
                    assert(p.rps.inGame, 'player should be inGame');
                });

                freshStart();
                //player2 picks first
                player2.socket.handleEvent(rps, choice1);
                player1.socket.handleEvent(rps, choice2);
                players.forEach((p) => {
                    assert(p.rps.inGame, 'player should be inGame');
                });
            };
            [[], [""], ["R", ""], ["S", ""], ["P", ""], ["", ""], [42, "P"]], []
                .forEach((choices) => { simulate(...choices) });
        });
    });
});