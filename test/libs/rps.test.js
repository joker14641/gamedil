require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const fakePlayer = require("../fake").player;
const createQueue = require("@root/libs/create-queue");

const createQueueStub = sinon.stub();
const rpsQueue = createQueue();
createQueueStub.returns(rpsQueue);

const chatStub = sinon.stub();
chatStub.sendMessage = sinon.stub();
const startGameStub = sinon.stub();

const rps = proxyquire("@root/libs/rps", {
    "./create-queue": createQueueStub,
    "./chat": chatStub,
    "./rps/start-game": startGameStub
});

const joinRPSQueue = "join-rps-queue";

const cleanUp = () => {
    rpsQueue._queue = [];
    sinon.resetHistory();
}

describe("rps", function () {
    afterEach(cleanUp);
    describe("connect(player)", function () {
        it("adds rps property to player", function () {
            const player = fakePlayer();
            rps.connect(player);
            expect(player).to.have.property("rps");
        });
        it("on emitting joinRPSqueue event, a connected player that is not in an RPS game will join the rps queue", function () {
            const player = fakePlayer();
            rps.connect(player);
            expect(player.socket._events).to.have.property(joinRPSQueue);
            assert.equal(createQueueStub(), rpsQueue);
            //player.socket._events[joinRPSQueue][0]();
            player.socket.handleEvent(joinRPSQueue);
            expect(rpsQueue._queue).to.contain(player);
        });
        it("a player that is in an RPS game can't queue for RPS", function () {
            const player = fakePlayer();
            rps.connect(player);
            player.rps.inGame = true;
            player.socket.handleEvent(joinRPSQueue);
            expect(rpsQueue._queue).to.not.contain(player);
        });
        it("when two players queue for rps, they will leave the queue and startGame will be called", function () {
            const players = [fakePlayer(), fakePlayer()];
            players.forEach(rps.connect);
            players.forEach((p) => {
                p.socket.handleEvent(joinRPSQueue);
            });
            players.forEach((p) => {
                expect(rpsQueue._queue).to.not.contain(p);
            });
            sinon.assert.calledOnce(startGameStub);
        });
    });
    describe("disconnect(player)", function () {
        it("unqueues the player", function () {
            const player = fakePlayer();
            rps.connect(player);
            player.socket.handleEvent(joinRPSQueue);
            rps.disconnect(player);
            expect(rpsQueue._queue).to.not.contain(player);
        });
    });
});