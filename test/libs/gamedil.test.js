require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const fakePlayer = require("../fake").player;
const createQueue = require("@root/libs/create-queue");

const createQueueStub = sinon.stub();
const gamedilQueue = createQueue();
createQueueStub.returns(gamedilQueue);

const chatStub = sinon.stub();
chatStub.sendMessage = sinon.stub();
const startGameStub = sinon.stub();

const createGamedilObjectStub = {
    createGamedilPlayer: ()=>({})
};

const gamedil = proxyquire("@root/libs/gamedil", {
    "./create-queue": createQueueStub,
    "./chat": chatStub,
    "./gamedil/start-game": startGameStub,
    "./gamedil/Gamedil-object": createGamedilObjectStub
});

const joinGamedilQueue = "join-gamedil-queue";


const cleanUp = () => {
    gamedilQueue._queue = [];
    sinon.resetHistory();
}

describe("gamedil", function () {
    afterEach(cleanUp);
    describe("connect(player)", function () {
        it("adds gamedil property to player", function () {
            const player = fakePlayer();
            gamedil.connect(player);
            expect(player).to.have.property("gamedil");
        });
        it("on emitting joinGamedilQueue event, a connected player that is not in an gamedil game will join the gamedil queue", function () {
            const player = fakePlayer();
            gamedil.connect(player);
            expect(player.socket._events).to.have.property(joinGamedilQueue);
            // assert.equal(createQueueStub(), gamedilQueue);
            player.socket.handleEvent(joinGamedilQueue);
            expect(gamedilQueue._queue).to.contain(player);
        });
        it("a player that is in a gamedil game can't queue for gamedil", function () {
            const player = fakePlayer();
            gamedil.connect(player);
            player.gamedil.inGame = true;
            player.socket.handleEvent(joinGamedilQueue);
            expect(gamedilQueue._queue).to.not.contain(player);
        });
        it("when two players queue for gamedil, they will leave the queue and startGame will be called", function () {
            const players = [fakePlayer(), fakePlayer()];
            players.forEach(gamedil.connect);
            players.forEach((p) => {
                p.socket.handleEvent(joinGamedilQueue);
            });
            players.forEach((p) => {
                expect(gamedilQueue._queue).to.not.contain(p);
            });
            sinon.assert.calledOnce(startGameStub);
        });
    });
    describe("disconnect(player)", function () {
        it("unqueues the player", function () {
            const player = fakePlayer();
            gamedil.connect(player);
            player.socket.handleEvent(joinGamedilQueue);
            gamedil.disconnect(player);
            expect(gamedilQueue._queue).to.not.contain(player);
        });
    });
});