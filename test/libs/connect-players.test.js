require('module-alias/register');
const proxyquire = require("proxyquire");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const chatStub = {};
const rpsStub = {};
const gamedilStub = {};
const channelStubs = [chatStub, rpsStub, gamedilStub];

const players = require("@root/libs/connected-players");
const { connect } = proxyquire("@root/libs/connect-players", {
    "./chat": chatStub,
    "./rps": rpsStub,
    "./gamedil": gamedilStub
});
channelStubs.forEach((c) => {
    c.connect = sinon.stub();
    c.disconnect = sinon.stub();
})

class FakeSocket {
    constructor(id) {
        this.id = id;
        this._events = {};
    }
    emit(event, ...args) {
        this._events[event].forEach(func => func(...args));
    }
    on(event, func) {
        if (this._events[event]) {
            return this._events[event].push(func);
        }
        this._events[event] = [func];
    }
    disconnect() {
        this.emit("disconnect");
    }
};


describe("connect-players", function () {
    describe("connect(socket) handles", function () {
        let socket, playerCountBefore;
        beforeEach(function () {
            channelStubs.forEach((c) => {
                c.connect.reset();
                c.disconnect.reset();
            });
            socket = new FakeSocket("testid");
            playerCountBefore = players.count;
        })
        describe("new connections", function () {
            it("updates connected-players", function () {
                expect(players).to.not.have.property(socket.id);
                connect(socket);
                expect(players.count).to.equal(playerCountBefore + 1);
                expect(players).to.have.property(socket.id);
                socket.disconnect();
            });

            it("connects with each channel exactly once", function () {
                connect(socket);
                const player = players[socket.id];
                channelStubs.forEach((c) => {
                    assert(c.connect.calledOnceWith(player));
                });
                socket.disconnect();
            });
        });
        describe("disconnects", function () {
            it("updates connected-players", function(){
                connect(socket);
                socket.disconnect();
                expect(players).to.not.have.property(socket.id);
                expect(players.count).to.equal(playerCountBefore);
                socket.disconnect();
            });
            it("disconnects from each channel exactly once", function () {
                connect(socket);
                const player = players[socket.id];
                socket.disconnect();
                channelStubs.forEach((c) => {
                    assert(c.disconnect.calledOnceWith(player));
                });
            });
        });

    });

});