require('module-alias/register');
const proxyquire = require("proxyquire");

const createPlayer = require("@root/libs/create-player");

const { expect, assert } = require('chai');
const sinon = require("sinon");

const ioStub = {
    "to": sinon.stub()
};
const ioToRoomStub = {
    "emit": sinon.stub()
};
class FakeSocket {
    constructor(id) {
        this.id = id;
        this._events = {};
        this.emit = sinon.stub();
        this.rooms = {};
        this.rooms[id] = {};
    }
    // emit(event, ...args) {
    //     this._events[event].forEach(func => func(...args));
    // }
    join(room) {
        this.rooms[room] = {};
    }
    on(event, func) {
        if (this._events[event]) {
            return this._events[event].push(func);
        }
        this._events[event] = [func];
    }
    disconnect() {
        this.emit("disconnect");
    }
};

const fakePlayer = function (id) {
    const socket = new FakeSocket(id);
    return createPlayer(socket);
}
const chat = proxyquire("@root/libs/chat", {
    "./server-constants": { io: ioStub }
});
const chatMessage = 'chat message';
const chatLobby = "chat-lobby";

const setUpStubs= ()=>{
    ioStub.to.reset();
    ioToRoomStub.emit.reset();
    ioStub.to.returns(ioToRoomStub);
};
describe("chat", function () {
    describe("connect(player)", function () {
        beforeEach(setUpStubs);
        it("sends a message to the lobby, that someone connected", function () {
            const player = fakePlayer("testid");
            chat.connect(player);
            sinon.assert.calledWith(ioStub.to, chatLobby);
        });
        it("if player has no name yet, he will get one upon sending a message and will join the lobby", function () {
            const player = fakePlayer("testid");
            chat.connect(player);
            expect(player).to.not.have.property("name");
            expect(player.socket.rooms).to.not.have.property(chatLobby);
            expect(player.socket._events).to.have.property(chatMessage);
            player.socket._events[chatMessage][0]("testname");
            expect(player).to.have.property("name");
            expect(player.name).to.equal("testname");
            expect(player.socket.rooms).to.have.property(chatLobby);

        });
        it("if player has a name, his chatmessages are sent to the lobby", function(){
            const player = fakePlayer("someId");
            chat.connect(player);
            player.socket._events[chatMessage][0]("testname");
            const msg = "some message";
            setUpStubs();
            player.socket._events[chatMessage][0](msg);
            
            sinon.assert.calledWith(ioStub.to, chatLobby);
            sinon.assert.calledWith(ioToRoomStub.emit, chatMessage, player.name+":\n"+msg);
        });
    });
    describe("disconnect(player)", function () { 
        beforeEach(setUpStubs);
        it("sends a message to the lobby, that someone disconnected", function(){
            const player = fakePlayer("dctestid");
            chat.disconnect(player);
            sinon.assert.calledWith(ioStub.to, chatLobby);
        });
    });
    describe("createRoom(purpose,player1,player2)", function () {
        it("causes both players to join the returned room", function(){
            const purpose = "some purpose";
            const player1 = fakePlayer("player1 id");
            const player2 = fakePlayer("player2 id");
            const players= [player1,player2];
            players.forEach((p)=>{
                sinon.spy(p.socket, "join"); 
            });            
            const room = chat.createRoom(purpose, ...players);
            players.forEach((p)=>{
                sinon.assert.calledWith(p.socket.join, room);
            });
        });
     });
    describe("sendMessage(room,msg)", function () {
        beforeEach(setUpStubs);
        it("sends msg as a chatmessage to room", function(){
            const room = "some room";
            const msg = "some msg";
            chat.sendMessage(room, msg);
            sinon.assert.calledWithExactly(ioStub.to, room);
            sinon.assert.calledWithExactly(ioToRoomStub.emit,chatMessage, msg);
        });
     });
});