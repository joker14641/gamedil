require('module-alias/register');
const createPlayer = require("@root/libs/create-player");

const sinon = require("sinon");

class FakeSocket {
    constructor(id) {
        this.id = id;
        this._events = {};
        this.emit = sinon.stub();
        this.rooms = {};
        this.rooms[id] = {};
        this.off = sinon.stub();
    }
    handleEvent(event, ...args) {
        this._events[event].forEach(func => func(...args));
    }
    join(room) {
        this.rooms[room] = {};
    }
    leave(room){
        delete this.rooms[room];
    }
    on(event, func) {
        if (this._events[event]) {
            return this._events[event].push(func);
        }
        this._events[event] = [func];
    }
    disconnect() {
        this.emit("disconnect");
    }
};

exports.socket = (id) => (new FakeSocket(id));

/**
 * generates a unique id
 */
const fakeId = (function () {
    let counter = 0;
    return () => {
        counter++;
        return "fakeid" + counter;
    };
})();

/**
 * generates a fakePlayer with unique id
 */
exports.player = () => {
    const id = fakeId();
    const socket = new FakeSocket(id);
    return createPlayer(socket);
};