import chat from "./chat.mjs"
import gamedil from "./gamedil.mjs"

(function(){
    const instructionsoverlay = document.getElementById("instructionsoverlay");
    instructionsoverlay.onclick = () => {
        instructionsoverlay.style.display = "none";
    };
    const instructionsbutton = document.getElementById("instructionsbutton");
    instructionsbutton.onclick = () => {
        instructionsoverlay.style.display = "block";
    };
})();

$(function () {
    const socket = io();
    chat(socket);
    gamedil(socket);
});
