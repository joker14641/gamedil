const messageDiv = document.getElementById("messages");
function updateScroll(){        
        messageDiv.scrollTop = messageDiv.scrollHeight;
}
const rpsQueueButton = document.getElementById("rps-queue-button");

export default function (socket) {
    const chat = "chat";
    const chatRex = /^\/chat/;
    const rps = "RPS";
    const rpsRex = /^\/rps/;
    const gamedil = "gamedil";
    const gamedilRex = /^\/gamedil/;
    const chatInput = $('#chat-input');
    const chatChannelDisplay = $("#chat-channel");
    let channel = chat;
    function showMessage(msg) {
        $('#messages').append($('<li>').text(msg));
        updateScroll();
    };
    $('form').submit(function () {
        let msg = chatInput.val();
        msg = msg.replace(/^\s*/, ""); // remove beginning whitespace
        const msgLower = msg.toLowerCase();
        if (/^\//.test(msgLower)) {
            if (chatRex.test(msgLower)) {
                channel = chat;
            } else if (rpsRex.test(msgLower)) {
                channel = rps;
            } else if(gamedilRex.test(msgLower)){
                channel = gamedil;
                socket.emit("join-gamedil-queue");
            } else {
                showMessage("Error: Illegal chat command.");
                chatInput.val('');
                return false;
            }
            chatChannelDisplay.text("To: "+channel);
            msg = msg.replace(/^\S*\s*/, ""); // remove first word and whitespace
        }

        if (channel === chat) {
            socket.emit('chat message', msg);
        } else if (channel === rps) {
            if (msg === "") {
                socket.emit("join-rps-queue");
            } else {
                msg = msg.toUpperCase();
                if (/^[RPS]/.test(msg)) {
                    msg = msg[0];
                    const selection = { R: "rock", P: "paper", S: "scissors" }[msg];
                    showMessage("You choose " + selection);
                    socket.emit("RPS", msg);
                } else {
                    showMessage("invalid selection. Please select Rock, Paper or Scissors (R/P/S).");
                }


            }

        }

        chatInput.val('');
        return false;
    });
    socket.on('chat message', showMessage);
	rpsQueueButton.onclick = () => { 
        channel = rps;
        chatChannelDisplay.text("To: "+rps);
        socket.emit("join-rps-queue");
     };
}
