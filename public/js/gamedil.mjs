const gamedilState = "gamedil-state";
const gamedilLoadData = "gamedil-load-data";
const gamedilLoaded = "gamedil-loaded";
const gamedilFailedToLoad = "gamedil-failed-to-load";
const gamedilLoadStatusInfo = "gamedil-load-status-info";
const gamedilStartGame = "gamedil-start-game";
const gamedilEndGame = "gamedil-end-game";
const gamedilYourTurnStarted = "gamedil-your-turn-started";
const gamedilYourTurnEnded = "gamedil-your-turn-ended";
const gamedilMoveInDirection = "gamedil-move-in-direction";
const gamedilStartCharging = "gamedil-start-charging";
const gamedilStopCharging = "gamedil-stop-charging";
const gamedilAimangle = "gamedil-aimangle";
const [left, stand, right] = [-1, 0, 1];
const directions = [left, stand, right];
const leftKeys = [65, 37];
const rightKeys = [68, 39];
const spaceBar = 32;
let ownId;
let ownPosition;
let haveTurn = false;
let moving = false;
let charging = false;

const gamediv = document.getElementById("gamediv");
gamediv.setAttribute("tabindex", "0");
const $gamediv = $("#gamediv");
const gamedilQueueButton = document.getElementById("gamedil-queue-button");

class GamedilObject {
    constructor(obj) {
        this.id = obj.id;
        this.height = obj.height;
        this.width = obj.width;
        this.canvas = document.createElement("canvas");
        this.canvas.style.position = "absolute";
        this.ctx = this.canvas.getContext("2d");
        gamediv.appendChild(this.canvas);
    }
    delete(){
        gamediv.removeChild(this.canvas);
    }
}

// class Game {
//     constructor(map, gamedilObjects){
//         this.map = map;
//         this.gamedilObjects = gamedilObjects;
//     }
//     start(){

//     }
// }

export default function (socket) {
    gamedilQueueButton.onclick = () => { socket.emit("join-gamedil-queue"); };
    const map = { };
    map.canvas = document.createElement("canvas");
    map.canvas.id = "mapcanvas";
    map.ctx = map.canvas.getContext("2d");
    map.canvas.style.position = "absolute";
    gamediv.appendChild(map.canvas);
    map.reset = () => {
        const {width, height} = map.canvas;
        map.ctx.clearRect(0,0,width,height );
    };
    const turncontrols = document.getElementById("turncontrols");
    let gamedilObjects = {};
    const aim = {
        div: document.createElement("div"),
        circle: {},
        arrow: {},
        angle: 0,
        angleInDeg: 0,
        orientation: 1
    };
    aim.circle.canvas = document.createElement("canvas");
    aim.circle.canvas.id = "aimcirclecanvas";
    aim.circle.canvas.style.position = "absolute";
    aim.circle.ctx = aim.circle.canvas.getContext("2d");
    aim.arrow.canvas = document.createElement("canvas");
    aim.arrow.canvas.id = "aimarrowcanvas";
    aim.arrow.ctx = aim.arrow.canvas.getContext("2d");
    aim.arrow.canvas.style.transformOrigin = "0% 50%";
    aim.arrow.canvas.style.position = "absolute";
    aim.div.id = "aimdiv";
    aim.div.style.position = "absolute";
    aim.div.appendChild(aim.circle.canvas);
    aim.div.appendChild(aim.arrow.canvas);
    aim.div.style.transformOrigin = "50% 50%";
    // [aim.div, aim.circle.canvas, aim.arrow.canvas].forEach(x => {
    //     x.style.backgroundColor = "rgba(0,0,0,0)";
    // });
    aim.div.style.display = "none";
    gamediv.appendChild(aim.div);

    const getWeapon = (() => {
        const weaponSelection = document.getElementById("weaponselection");
        return () => {
            const selection = weaponSelection.querySelector('input[name="weapon"]:checked');
            if (selection) {
                return selection.value;
            }
            return "cannon";
        };
    })();

    const bullets = {
        types: []
    };
    bullets.hide = () => {
        bullets.types.forEach(type => {
            bullets[type].canvas.style.display = "none";
        })
    }
    const charge = {
        bar: document.getElementById("chargebar"),
        container: document.getElementById("chargecontainer")
    };

    /**
     * loads data and informs the server when it is done
     * @param loadData contains paths to ressources to be loaded
     */
    async function load(loadData) {
        console.log("loading ");

        //unloading data from previous game
        for (const key in gamedilObjects) {
            if (gamedilObjects.hasOwnProperty(key)) {
                const element = gamedilObjects[key];
                element.delete();
            }
        }
        gamedilObjects = {};
        map.reset();
        const mapPromise = new Promise((resolve, reject) => {
            const img = new Image();
            img.onload = () => {
                map.canvas.width = map.width = img.width;
                map.canvas.height = map.height = img.height;
                map.ctx.drawImage(img, 0, 0);
                gamediv.style.height = map.height + "px";
                resolve();
            };
            img.onerror = reject;
            img.src = "graphics/" + loadData.map;
        });
        const gamedilObjectsPromises = loadData.objects.map(obj =>
            new Promise((resolve, reject) => {
                const gamedilObj = gamedilObjects[obj.id] = new GamedilObject(obj);
                const img = new Image();
                img.onload = () => {
                    gamedilObj.canvas.width = img.width;
                    gamedilObj.canvas.height = img.height;
                    gamedilObj.ctx.drawImage(img, 0, 0);
                    console.log("loaded obj:", obj, "img.w,h:", img.width, img.height, img.src);
                    resolve();
                };
                img.onerror = reject;
                img.src = "graphics/" + obj.src;
            })
        );
        const aimPromises = ["circle", "arrow"].map(x =>
            new Promise((resolve, reject) => {
                const img = new Image();
                img.onload = () => {
                    aim[x].canvas.width = img.width;
                    aim[x].canvas.height = img.height;
                    aim[x].img = img;
                    resolve();
                    console.log("loaded aim" + x, img.src);
                };
                img.onerror = reject;
                img.src = "graphics/aim/" + loadData.aim[x];
            })
        );
        const bulletPromises = loadData.bullets.map(([type, { src, size }]) => {
            const canvas = document.createElement("canvas");
            const ctx = canvas.getContext("2d");
            canvas.style.position = "absolute";
            canvas.style.display = "none";
            canvas.style.transformOrigin = "50% 50%";
            gamediv.appendChild(canvas);
            bullets[type] = { canvas, ctx };
            bullets.types.push(type);
            return new Promise((resolve, reject) => {
                const img = new Image();
                img.onload = () => {
                    bullets[type].canvas.width = bullets[type].canvas.height = size;
                    bullets[type].ctx.drawImage(img, 0, 0, size, size);
                    bullets[type].radius = 0.5 * size;
                    resolve();
                };
                img.onerror = reject;
                img.src = "graphics/bullets/" + src;
            });
        });
        const promises = [mapPromise, ...gamedilObjectsPromises, ...aimPromises, ...bulletPromises];
        try {
            await Promise.all(promises);
            console.log("loaded");
            socket.emit(gamedilLoaded);
            const selfHeight = gamedilObjects[ownId].height;
            [aim.div, aim.circle.canvas].forEach(x => {
                x.width = 2 * selfHeight;
                x.height = 2 * selfHeight;
            });
            aim.arrow.canvas.width = selfHeight;
            aim.arrow.canvas.height = 10;
            ["circle", "arrow"].forEach(x => {
                aim[x].ctx.clearRect(0, 0, aim[x].canvas.width, aim[x].canvas.height);
                // aim[x].ctx.beginPath();
                aim[x].ctx.drawImage(aim[x].img, 0, 0, aim[x].canvas.width, aim[x].canvas.height);
                // aim[x].ctx.closePath();
                // aim[x].canvas.style.backgroundColor = "rgba(0,0,0,0)";
            });
        } catch (error) {
            console.log("failed", error);
            socket.emit(gamedilFailedToLoad);
        }

    }
    socket.on(gamedilLoadData, (data, id) => {
        // console.log("loaddata: ", data); 
        ownId = id;
        load(data);
    });
    const gameState = {};
    socket.on(gamedilState, (state) => {
        for (const mapChange of state.mapChanges) {
            switch (mapChange.type) {
                case "explosion":
                    const ctx = map.ctx;
                    const { x, y, radius } = mapChange;

                    //coordinates for square to be updated
                    const sx = x - radius;
                    const sy = y - radius;
                    const w = 2 * radius;

                    ctx.save();
                    ctx.beginPath();
                    ctx.arc(x, y, radius, 0, 2 * Math.PI);
                    ctx.closePath();
                    ctx.clip();
                    ctx.clearRect(sx, sy, w, w);
                    ctx.restore();
                    break;
                default:
                    break;
            }
        }
        gameState.objects = state.objects;
        gameState.bulletsInAir = state.bulletsInAir;
    });

    let renderRequest;
    const render = () => {
        gameState.objects.forEach(obj => {
            if (obj.id === ownId) {
                ownPosition = obj.position;
            }
            gamedilObjects[obj.id].canvas.style.transform = `translate(${obj.position.x}px, ${obj.position.y + 0}px)`; //"translate(" + obj.x +"px," + (obj.y+0)+"px)";
        });
        if (haveTurn) {
            // aim.div.style.display = "inline";
            const { x, y } = ownPosition;
            const { height, width } = gamedilObjects[ownId];
            const orientation = aim.orientation;
            aim.circle.canvas.style.transform = "translate(" + (x - height + 0.5 * width) + "px, " + (y + 10 - 0.5 * height) + "px) scaleX(" + orientation + ")";
            aim.arrow.canvas.style.transform = "translate(" + (x + 0.5 * width) + "px, " + (y + 10 + 0.5 * (height - aim.arrow.canvas.height)) + "px) rotate(" + aim.angleInDeg + "deg) scaleY(" + orientation + ")";
            if (charging) {
                const chargeTime = Date.now() - charge.startedAt;
                const power = Math.min(1, chargeTime / 3000);
                const maxWidth = 400;
                charge.bar.style.transform = "scale(" + power * maxWidth + ",1)";
            }
        }
        bullets.hide();
        gameState.bulletsInAir.forEach(bullet => {
            const {
                type,
                position: { x, y },
                angleInDeg,
                orientation
            } = bullet;
            const radius = bullets[type].radius;
            bullets[type].canvas.style.display = "inline";
            bullets[type].canvas.style.transform =
                "translate(" + (x - radius) + "px," + (y - radius + 0) + "px) "
                + "rotate(" + angleInDeg + "deg) scaleY(" + orientation + ")";
        });
        renderRequest = requestAnimationFrame(render);
    };
    let leftMouseButtonDown = false;
    let mouseX = 0;
    let mouseY = 0;
    gamediv.onmousedown = (event) => {
        if (event.button === 0) {
            leftMouseButtonDown = true;
            //console.log("mouse down");
        }
    };
    const $document = $(document);
    gamediv.onmousemove = function (event) {
        mouseX = event.clientX + $document.scrollLeft();
        mouseY = event.clientY + $document.scrollTop();
        if (haveTurn && leftMouseButtonDown) {
            const self = gamedilObjects[ownId];
            const { x, y } = ownPosition;
            const angle = Math.atan2(mouseY - y - 0.5 * self.height, mouseX - x - 0.5 * self.width);
            if (charging) {
                socket.emit(gamedilAimangle, angle);
            }
            aim.angle = angle;
            aim.angleInDeg = angle * 180 / Math.PI;
            aim.orientation = (mouseX >= x + 0.5 * self.width) ? 1 : -1;
        }
        // console.log("moving mouse");
    };
    document.body.onmouseup = () => {
        leftMouseButtonDown = false;
        //console.log("mouse released");
    };

    socket.on(gamedilStartGame, render);
    socket.on(gamedilEndGame, () => {
        console.log("game ended");
        cancelAnimationFrame(renderRequest);
    });
    socket.on(gamedilLoadStatusInfo, (info) => {
        console.log("Loadstatus:", info);
    });
    socket.on(gamedilYourTurnStarted, () => {
        haveTurn = true;
        moving = true;
        aim.div.style.display = "inline";
        // charge.container.style.display = "block";
        turncontrols.style.display = "block";
    });
    socket.on(gamedilYourTurnEnded, () => {
        haveTurn = false;
        moving = false;
        charging = false;
        aim.div.style.display = "none";
        // charge.container.style.display = "none";
        turncontrols.style.display = "none";
    });

    //handle keyboard controls
    $gamediv.keydown(e => {
        // keysDown[e.keyCode] = true;
        if (haveTurn) {
            if (moving) {
                if (e.keyCode === spaceBar) {
                    moving = false;
                    charging = true;
                    const weapon = getWeapon();
                    socket.emit(gamedilStartCharging, weapon);
                    socket.emit(gamedilAimangle, aim.angle);
                    charge.startedAt = Date.now();
                }
                if (leftKeys.includes(e.keyCode)) {
                    socket.emit(gamedilMoveInDirection, left);
                }
                if (rightKeys.includes(e.keyCode)) {
                    socket.emit(gamedilMoveInDirection, right);
                }
            }
        }
    });
    $gamediv.keyup(e => {
        if (haveTurn) {
            if (leftKeys.includes(e.keyCode) || rightKeys.includes(e.keyCode)) {
                socket.emit(gamedilMoveInDirection, stand);
            }
        }
    });
    $(window).keyup(e => {
        if (haveTurn && charging) {
            if (e.keyCode === spaceBar) {
                socket.emit(gamedilStopCharging);
                charging = false;
            }
        }
    });
    $gamediv.focusout(() => {

    });
}
