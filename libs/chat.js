const { io } = require("./server-constants");
const players = require("./connected-players");
const chatMessage = 'chat message';
const chatLobby = "chat-lobby";

exports.connect = function (player) {
    const socket = player.socket;
    io.to(chatLobby).emit(chatMessage, `Someone connected. 
    There are currently ${players.count} users connected, 
    including you.`);
    socket.emit(chatMessage, "Please enter a name.");
    socket.on(chatMessage, function (msg) {
        if (socket.rooms[chatLobby]) {
            io.to(chatLobby).emit(chatMessage, player.name + ":\n" + msg);
        }
        else {
            player.name = msg;
            socket.join(chatLobby);
            io.to(chatLobby).emit(chatMessage, player.name + " has joined the chat.");
        }
    });
};
exports.disconnect = function (player) {
    io.to(chatLobby).emit(chatMessage, player.name + ` has disconnected. 
    There are currently ${players.count} users connected, 
    including you.`);
};

/**
 * creates a private room that members will join
 * @param purpose a string
 * @param player1, @param player2 the players to join
 * @returns the id of the created room
 */
exports.createRoom = function (purpose, player1, player2) {
    const room = purpose + player1.id + player2.id;
    [player1, player2].forEach(function (p) {
        p.socket.join(room)
    });
    return room;
}

exports.sendMessage = function(room, msg){
    io.to(room).emit(chatMessage, msg);
};
