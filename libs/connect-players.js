const createPlayer = require("./create-player");
const players = require("./connected-players");
const chat = require("./chat");
const rps = require("./rps");
const gamedil = require("./gamedil");

exports.connect = function (socket) {
    players.count++;
    const player = createPlayer(socket);
    players[player.id] = player;
    const channels = [chat, rps, gamedil];
    channels
        .forEach((c) => {c.connect(player);});

    socket.on('disconnect', function () {
        //console.log('user disconnected');
        players.count--;
        channels
            .forEach((c) => {c.disconnect(player);});
            
        delete players[player.id];
    });
};
