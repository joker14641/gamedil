const queue = (require("./create-queue"))();
const chat = require("./chat");
const startGame = require("./rps/start-game");
const joinRPSQueue = "join-rps-queue";

exports.connect = function (player) {
    player.rps = {};
    const socket = player.socket;
    socket.on(joinRPSQueue, function () {        
        if (!player.rps.inGame) {
            //console.log("a user joined RPSQueue");
            player.joinQueue(queue);
            chat.sendMessage(player.id, "You joined the RPS-Queue.");
            if (queue.length() >= 2) {
                startGame(queue.shift(), queue.shift());
            }
        }
    });
};

exports.disconnect = function (player) {
    queue.remove(player);
};

