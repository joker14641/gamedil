const queue = (require("./create-queue"))();
const chat = require("./chat");
const startGame = require("./gamedil/start-game");
const {createGamedilPlayer} = require("./gamedil/Gamedil-object");

const joinGamedilQueue = "join-gamedil-queue";


exports.connect = function (player) {
    const socket = player.socket;
    player.gamedil = createGamedilPlayer("hero.png");
    socket.on(joinGamedilQueue, function () {
        if (!player.gamedil.inGame) {
             console.log("a user joined Gamedil-Queue");
            player.joinQueue(queue);
            chat.sendMessage(player.id, "You joined the Gamedil-Queue.");
            if (queue.length() >= 2) {
                startGame(queue.shift(), queue.shift());
            }
        }
    });
}

exports.disconnect = function (player) {
    queue.remove(player);
}