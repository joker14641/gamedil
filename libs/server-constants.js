const express = require("express");
const app = express();
const http = require('http').Server(app);
const io = require("socket.io")(http);
const port = process.env.PORT || 3000;

module.exports = {
    "express": express,
    "app": app,
    "http": http,
    "io": io,
    "port": port
};
