class Player{
    constructor(socket){
        this.socket = socket;
        this.id = socket.id;
    }
    joinQueue(queue){
        return queue.push(this);
    }
    leaveQueue(queue){
        queue.remove(this);
    }
}

module.exports = (socket) => new Player(socket);