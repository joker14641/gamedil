class Queue{
    constructor(){
        this._queue = [];
    }
    length(){
        return this._queue.length;
    }
    push(obj){
        const queue = this._queue;
        if(!(queue.includes(obj))){
            queue.push(obj);
            return true;
        }
        return false;
    }
    shift(){
        return this._queue.shift();
    }
    remove(obj){
        const queue = this._queue;
        let i = queue.indexOf(obj);
        if(i>-1){
            queue.splice(i, 1);
        }
    }
};

module.exports = ()=> (new Queue());