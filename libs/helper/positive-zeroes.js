/**
 *  returns an Array containing all strictly positive solutions of ax^2+bx+c = 0
 * @param {number} a 
 * @param {number} b 
 * @param {number} c 
 */
module.exports = (a, b, c) => {
    if (a === 0) {
        if (b === 0 || c === 0)
            return [];
        const x = -c / b;
        if (x > 0)
            return [x];
        return [];
    }
    const Dsquared = b * b - 4 * a * c;
    if (Dsquared < 0)
        return [];
    const D = Math.sqrt(Dsquared);
    const x1 = 0.5 / a * (-b + D);
    const x2 = 0.5 / a * (-b - D);
    return [x1, x2].filter( x => x > 0);
};