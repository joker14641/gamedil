/**
 *  returns Infinity or the smallest strictly positive solution of ax^2+bx+c = 0
 * @param {number} a 
 * @param {number} b 
 * @param {number} c 
 */
module.exports = (a, b, c) => {
    if (a === 0) {
        if (b === 0 || c === 0)
            return Infinity;
        const x = -c / b;
        if (x > 0)
            return x;
        return Infinity;
    }
    const Dsquared = b * b - 4 * a * c;
    if (Dsquared < 0)
        return Infinity;
    const D = Math.sqrt(Dsquared);
    const x1 = 0.5 / a * (-b + D);
    const x2 = 0.5 / a * (-b - D);
    if (x1 > 0) {
        if (x1 <= x2 || x2 <= 0)
            return x1;
        return x2;
    }
    if (x2 > 0)
        return x2;
    return Infinity;
};