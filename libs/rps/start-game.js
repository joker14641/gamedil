const chat = require("../chat");
const rps = "RPS";

module.exports = function startGame(player1, player2) {
    const players = [player1, player2];
    const room = chat.createRoom("RPS", player1, player2);
    players.forEach(function (p) {
        p.rps.inGame = true;
    });
    chat.sendMessage(room, "RPS: Game started. Please pick rock, paper, or scissors.");
    const [rock, paper, scissors] = ["R", "P", "S"];
    const selections = { count: 0 };
    /**
     * @returns 0 if draw, 1 if player1 wins, 2 if player2 wins
     */
    function findWinner() {
        const s1 = selections[player1.id];
        const s2 = selections[player2.id];
        if (s1 === s2) {
            return 0;
        }
        if (s1 === rock && s2 === scissors) {
            return 1;
        }
        if (s1 === paper && s2 === rock) {
            return 1;
        }
        if (s1 === scissors && s2 === paper) {
            return 1;
        }
        return 2;
    }
    players.forEach(function (p) {
        p.rps.rpsEventHandler = function (msg) {
            if (p.rps.inGame) {
                if (selections.hasOwnProperty(p.id)) {
                    chat.sendMessage(p.id, "RPS: You already made your choice.");
                }
                else if (!["R", "P", "S"].includes(msg)) {
                    chat.sendMessage(p.id, "RPS: Pick rock, paper, or scissors!");
                }
                else {
                    selections[p.id] = msg;
                    selections.count++;
                    if (selections.count === 2) {
                        chat.sendMessage(room, `RPS: ${player1.name} picked ${selections[player1.id]}, ${player2.name} picked ${selections[player2.id]}`);
                        const winner = findWinner();

                        if (winner === 0) {
                            chat.sendMessage(room, "RPS: Game ends in a draw.")
                        }
                        else {
                            chat.sendMessage(room, `RPS: ${players[winner-1].name} wins.`)
                        }
                        players.forEach(function (q) {
                            q.rps.inGame = false;
                            q.socket.leave(room);
                            q.socket.off(rps, q.rps.rpsEventHandler);
                            delete q.rps.rpsEventHandler;
                        });
                    }
                }
            }
        }
        p.socket.on(rps, p.rps.rpsEventHandler);
    })

}