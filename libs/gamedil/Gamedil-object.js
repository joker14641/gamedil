const positiveZeroes = require("../../libs/helper/positive-zeroes");

class Position {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    set(x, y) {
        this.x = x;
        this.y = y;
    }
}

class GamedilObject {
    constructor(src) {
        this.src = src;
        this.inGame = false;
        // this.leftCollisionPoints = [new Position(0,0)];
        // this.rightCollisionPoints = [new Position(0,0)];
        // this.topCollisionPoints = [new Position(0,0)];
        // this.bottomCollisionPoints = [new Position(0,0)];
        this.position = new Position();
    }

    get state() {
        return {
            id: this.id,
            position: this.position
        };
    }

    get loadData() {
        return {
            id: this.id,
            src: this.src,
            width: this.width,
            height: this.height
        };
    }

    get left() {
        return this.position.x;
    }
    get right() {
        return this.position.x + this.width;
    }
    get top() {
        return this.position.y;
    }
    get bottom() {
        return this.position.y + this.height;
    }

    collisionsWith(position) {
        const { x, y } = position;
        const { left, right, top, bottom } = this;

        if (x < left || x > right || y < top || y > bottom) {
            return [];
        }
        return [{
            type: this.collisionType,
            info: this.collisionInfo
        }];
    }

    timeWithNoCollisions(motionData) {
        const { left, right, top, bottom } = this;
        const {
            position: { x, y },
            velocity: { vx, vy },
            acceleration: { ax, ay }
        } = motionData;

        const potentialLeftCollisiontimes = positiveZeroes(0.5 * ax, vx, x - left);
        const potentialRightCollisiontimes = positiveZeroes(0.5 * ax, vx, x - right);
        const potentialTopCollisiontimes = positiveZeroes(0.5 * ay, vy, y - top);
        const potentialBottomCollisiontimes = positiveZeroes(0.5 * ay, vy, y - bottom);

        const verticalCollisiontimes = [...potentialLeftCollisiontimes, ...potentialRightCollisiontimes]
            .filter(time => {
                const yThen = y + time * (vy + 0.5 * ay * time);
                return yThen >= top && yThen <= bottom;
            });

        const horizontalCollisiontimes = [...potentialTopCollisiontimes, ...potentialBottomCollisiontimes]
            .filter(time => {
                const xThen =  x + time * (vx + 0.5 * ax * time);
                return xThen >= left && xThen <= right;
            });
            
        return Math.min(...verticalCollisiontimes, ...horizontalCollisiontimes);
    }

}

class GamedilPlayer extends GamedilObject {
    constructor(src) {
        super(src);
        this.width = 32;
        this.height = 48;
        const collisionPoints = new Set();
        const n = 10;
        for (let i = 0; i <= n; i++) {
            collisionPoints
                .add(new Position(0, Math.floor(this.height * i / n)))
                .add(new Position(this.width, Math.floor(this.height * i / n)))
                .add(new Position(Math.floor(this.width * i / n), 0))
                .add(new Position(Math.floor(this.height * i / n), this.height));
        };
        this.collisionPoints = collisionPoints;
        this.aimAngle = 0;
        this.collisionType = "playerCollision";
    }
    get collisionInfo() {
        return { player: this };
    }

    clone(){
        const src = this.src;
        return new GamedilPlayer(src);
    }
}

/**
 * creates a new GamedilObject
 *  @param {String} src source of the object's image
 */
exports.createGamedilObject = (src) => new GamedilObject(src);

/**
 * creates a new GamedilPlayer
 * @param {String} src source of player's image 
 */
exports.createGamedilPlayer = (src) => new GamedilPlayer(src);
