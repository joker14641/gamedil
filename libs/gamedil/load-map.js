const { createCanvas, loadImage } = require("canvas");
const smallestPositiveZero = require("@root/libs/helper/smallest-positive-zero");
const updateMap = require("./update-map");
const solid = 2;
const mixed = 1;
const air = 0;
class MapDatum {
    constructor(data, x, y, size) {
        this.data = data;
        this.x = x;
        this.y = y;
        this.size = size;
    }
}

class Map {
    constructor(canvas, ctx) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.height = canvas.height;
        this.width = canvas.width;
        this.layer = [];
        this.gravity = 0.0001; //in pixel/(ms^2)
        this.wind = 0;
        this.stateChanges = [];
    }
    initialize() {
        const width = this.width;
        const height = this.height;
        const layer = this.layer;
        const imgData = this.ctx.getImageData(0, 0, this.width, this.height).data;
        layer.push(Array(this.width).fill(0).map((x) => [])); //initialize layer[0] with an array of empty arrays
        const layer0 = layer[0];

        //initialize layer0 with the data from the image
        for (let y = 0, i = 0; y < height; y++) {
            for (let x = 0; x < width; x++ , i += 4) {
                if ( // pixel is transparent or white
                    imgData[i + 3] < 255 || //alpha of pixel (x,y)
                    (imgData[i] == 255 && // red
                        imgData[i + 1] == 255 && // green
                        imgData[i + 2] == 255 // blue
                    )
                ) {
                    layer0[x][y] = new MapDatum(air, x, y, 1);
                }
                else {
                    layer0[x][y] = new MapDatum(solid, x, y, 1);
                }
            }
        }

        //compute subsequent layers, as long as their sizes aren't too large
        const minDim = Math.min(width, height);
        for (let depth = 1, prevSize = 1, curSize = 2; curSize <= minDim; depth++ , prevSize = curSize, curSize *= 2) {
            layer[depth] = Array(this.width).fill(0).map((x) => []); //push a new layer
            const prevLayer = layer[depth - 1];
            const curLayer = layer[depth];
            for (let x = 0; x < width; x += curSize) {
                const curLayerx = curLayer[x];
                const prevLayerx = prevLayer[x];
                for (let y = 0; y < height; y += curSize) {
                    const dataOnPrevLayer = {};
                    [air, solid, mixed].forEach((x) => dataOnPrevLayer[x] = 0);
                    dataOnPrevLayer[prevLayerx[y].data]++;
                    if (y + prevSize < height) {
                        dataOnPrevLayer[prevLayerx[y + prevSize].data]++;
                        if (x + prevSize < width) {
                            dataOnPrevLayer[prevLayer[x + prevSize][y + prevSize].data]++;
                        }
                    }
                    if (x + prevSize < width) {
                        dataOnPrevLayer[prevLayer[x + prevSize][y].data]++;
                    }
                    if (dataOnPrevLayer[mixed] > 0 || (dataOnPrevLayer[air] > 0 && dataOnPrevLayer[solid] > 0)) {
                        curLayerx[y] = new MapDatum(mixed, x, y, curSize);
                    } else if (dataOnPrevLayer[air] > 0) {
                        curLayerx[y] = new MapDatum(air, x, y, curSize);
                    } else if (dataOnPrevLayer[solid] > 0) {
                        curLayerx[y] = new MapDatum(solid, x, y, curSize);
                    }

                    //fill remainder of curLayer with convenient references
                    for (let sx = x; sx < Math.min(width, x + curSize); sx++) {
                        for (let sy = y; sy < Math.min(height, y + curSize); sy++) {
                            curLayer[sx][sy] = curLayer[x][y];
                        }
                    }

                }
            }


        }


    }
    update(x, y, w, h) {
        updateMap(this, x, y, w, h);
    }
    /**
     * @param {*} x 
     * @param {*} y 
     * @param {*} layer optional, defaults to 0
     * @returns {boolean} is the area at position x,y on map.layer[layer] air?
     */
    isAir(x, y, layer = 0) {
        x = Math.floor(x);
        y = Math.floor(y);
        if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
            return false;
        }
        return this.layer[layer][x][y].data === air;
    }

    /**
     * is the gamedilObj out of boundary?
     * @param {*} gamedilObj 
     */
    isOutOfBoundary(gamedilObj) {
        const { x, y } = gamedilObj.position;
        const isOutOfBounds = (x, y) => x < 0 || x >= this.width || y < 0 || y >= this.height;
        for (const cP of gamedilObj.collisionPoints) {
            if (isOutOfBounds(x + cP.x, y + cP.y)) {
                return true;
            }
        }
        return false;
    }
    /**
     * detect if position collides with the map. also optimizes the positions mapLayer property
     * @param {*} position 
     * @returns an empty array if no collision, otherwise an array with the type of collisions
     */
    collisionsWith(position) {
        let { x, y, mapLayer: k } = position;
        if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
            return [{ type: "mapBoundaryCollision" }];
        }
        x = Math.floor(x);
        y = Math.floor(y);
        if (this.layer[0][x][y].data === solid) {
            return [{ type: "mapCollision" }];
        }

        //optimize mapLayer
        if (this.layer[k][x][y].data === mixed) {
            k--;
            while (this.layer[k][x][y].data === mixed) {
                k--;
            }
        }
        else {
            while (k + 1 < this.layer.length && this.layer[k + 1][x][y].data === air) {
                k++;
            }
        }
        position.mapLayer = k;

        return [];
    }

    /**
     * 
     * @param {*} motionData 
     */
    timeWithNoCollisions(motionData) {
        const { width, height, gravity: g, wind: w } = this;
        const {
            position: { x, y, mapLayer: k },
            velocity: { vx, vy }
        } = motionData;
        const square = this.layer[k][Math.floor(x)][Math.floor(y)];
        const { x: cx, y: cy, size } = square;
        const collisionepsilon = 0.1;
        const dleft = cx - x - collisionepsilon; // oriented distance to left boundary, the epsilon makes sure that we really end up in a different square
        const dright = Math.min(dleft + 2 * collisionepsilon + size, width - x);
        const dtop = cy - y - collisionepsilon;
        const dbottom = Math.min(dtop + 2 * collisionepsilon + size, height - y);

        //how long until P will hit the top boundary
        const ttop = smallestPositiveZero(g, 2 * vy, -2 * dtop);
        //when will P hit the bottom boundary
        const tbottom = smallestPositiveZero(g, 2 * vy, -2 * dbottom);
        //when will P hit the left boundary
        const tleft = smallestPositiveZero(w, 2 * vx, -2 * dleft);
        //when will P hit the right boundary
        const tright = smallestPositiveZero(w, 2 * vx, -2 * dright);

        let t = Math.min(ttop, tbottom, tleft, tright);
        return isFinite(t) ? t : 1;
    }

    explodeBulletAt(x, y, explosionRadius) {
        this.stateChanges.push({
            type: "explosion",
            x,
            y,
            radius: explosionRadius
        });
        const ctx = this.ctx;

        //coordinates for square to be updated
        const sx = x - explosionRadius;
        const sy = y - explosionRadius;
        const w = 2 * explosionRadius;

        ctx.save();
        ctx.beginPath();
        ctx.arc(x, y, explosionRadius, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.clip();
        ctx.clearRect(sx, sy, w, w);
        ctx.restore();
        this.update(sx, sy, w, w);
    }

    get state() {
        const changes = this.stateChanges;
        this.stateChanges = [];
        return changes;
    }
}

/**
 * 
 * @param {string} src 
 * @returns {Promise<Map>} promise that will resolve to the initialized map or reject if image couldn't be loaded
 */
async function loadMap(src) {
    const img = await loadImage("public/graphics/" + src);
    const canvas = createCanvas(img.width, img.height);
    const ctx = canvas.getContext("2d");
    canvas.width = img.width;
    canvas.height = img.height;
    ctx.drawImage(img, 0, 0);
    const map = new Map(canvas, ctx);
    map.initialize();
    return (map);

}

module.exports = loadMap;