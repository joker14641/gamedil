const solid = 2;
const mixed = 1;
const air = 0;

/**
 * update map in the w*h rectangle starting in (x,y)
 * @param {*} map 
 * @param {number} x x-coordinate of the upper left corner of the rectangle
 * @param {number} y y-coordinate of the upper left corner of the rectangle
 * @param {number} w width of the rectangle
 * @param {number} h height of the rectangle
 */
const updateMap = (map, x, y, w, h) => {
    const width = map.width;
    const height = map.height;
    const layer = map.layer;
    x = Math.floor(x);
    y = Math.floor(y);
    w = Math.floor(w + 1);
    h = Math.floor(h + 1);
    if (w <= 0 || h <= 0 || x >= width || y >= height || x + w <= 0 || y + h <= 0)
        return;

    if (x < 0) {
        w += x;
        x = 0;
    }
    if (y < 0) {
        h += y;
        y = 0;
    }
    if (x + w > width) {
        w = width - x;
    }
    if (y + h > height) {
        h = height - y;
    }

    //update map.layer[0]
    const layer0 = layer[0];
    const imgData = map.ctx.getImageData(x, y, w, h).data;
    for (let v = y, i = 0; v < y+h; v++) {
        for (let u = x; u < x+w; u++ , i += 4) {
            if ( // pixel is transparent or white
                imgData[i + 3] < 255 || //alpha of pixel (x,y)
                (imgData[i] == 255 && // red
                    imgData[i + 1] == 255 && // green
                    imgData[i + 2] == 255 // blue
                )
            ) {
                layer0[u][v].data = air;
            }
            else {
                layer0[u][v].data = solid;
            }
        }
    }

    //update other layers
    for (let depth = 1, prevSize = 1, curSize = 2; depth < layer.length; depth++ , prevSize = curSize, curSize *= 2) {
        const prevLayer = layer[depth - 1];
        const curLayer = layer[depth];
        
        const cx = curLayer[x][y].x;
        const cy = curLayer[x][y].y;
        const umax = Math.min(cx+w+curSize, width);
        for (let u = cx; u < umax; u += curSize) {
            const curLayeru = curLayer[u];
            const prevLayeru = prevLayer[u];
            const vmax = Math.min(cy+h+curSize,height);
            for (let v = cy; v < vmax; v += curSize) {
                const dataOnPrevLayer = {};
                [air, solid, mixed].forEach((x) => dataOnPrevLayer[x] = 0);
                dataOnPrevLayer[prevLayeru[v].data]++;
                if (v + prevSize < height) {
                    dataOnPrevLayer[prevLayeru[v + prevSize].data]++;
                    if (u + prevSize < width) {
                        dataOnPrevLayer[prevLayer[u + prevSize][v + prevSize].data]++;
                    }
                }
                if (u + prevSize < width) {
                    dataOnPrevLayer[prevLayer[u + prevSize][v].data]++;
                }
                if (dataOnPrevLayer[mixed] > 0 || (dataOnPrevLayer[air] > 0 && dataOnPrevLayer[solid] > 0)) {
                    curLayeru[v].data = mixed;
                } else if (dataOnPrevLayer[air] > 0) {
                    curLayeru[v].data =air;
                } else if (dataOnPrevLayer[solid] > 0) {
                    curLayeru[v].data = solid;
                }
            }
        }


    }
}
module.exports = updateMap;