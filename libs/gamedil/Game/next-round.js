const turn = require("./turn");

/**
 * sorts players by their delay
 * @param {*} players array of players
 */
const sort = (players) => {
    players.sort((p, q) => p.gamedil.delay - q.gamedil.delay);
}

/**
  * advances the game by one round
  * @param {Game} game 
  * @returns {Promise<any>} promise that resolves when the new round is over
  */
const nextRound = async (game) => {
    game.round++;
    if (game.over()) {
        return;
    }
    // console.log("Round " + game.round);
    const { players, room, map } = game;
    sort(players);
    for (const p of players) {
        try {
            await turn(game, p);
        } catch (error) {
            console.log(error, __filename);
            continue;
        }
        if (game.over()) {
            return;
        }
    }
};

module.exports = nextRound;