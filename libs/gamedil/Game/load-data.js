module.exports = (game) => {
    const loadData = {
        map: game.mapSrc,
        objects: game.objects.map(gamedilObj => gamedilObj.loadData),
        aim: {
            arrow: "arrow.png",
            circle: "circle.png"
        },
        bullets: [...game.bullets]
            .map(([type, { src, size }]) =>
                [type, { src, size }])
    };
    return loadData;
};