const { io } = require("../../server-constants");
const nextRound = require("./next-round");
const load = require("./load");
const currentState = require("./current-state");
const loadData = require("./load-data");
const shoot = require("./shoot");

const gamedilState = "gamedil-state";
const gamedilLoadData = "gamedil-load-data";
const gamedilLoadStatusInfo = "gamedil-load-status-info";
const gamedilStartGame = "gamedil-start-game";
const gamedilEndGame = "gamedil-end-game";
const gamedilYourTurnStarted = "gamedil-your-turn-started";
const gamedilYourTurnEnded = "gamedil-your-turn-ended";

class Game {
    constructor(players, room, mapSrc) {
        this.players = players;
        players.forEach(function (p) {
            p.gamedil.inGame = true;
        });
        this.objects = players.map( p => p.gamedil);
        this.room = room;
        this.mapSrc = mapSrc;
        this.supportedWeapons = ["grenade", "cannon"];
        const standardBulletSize = 30;
        this.bullets = new Map([
            ["cannon", {src: "fish.png", size: standardBulletSize}],
            ["grenade", {src: "bullet.png",size: standardBulletSize}]
        ]);
        this.bulletsInAir = [];
        this.round = 0;

        /**
         * this is an internal method used to generate an id that is unique during the game and is used to identify gamedilObjects.
         */
        this._generateId = (function () {
            let counter = 0;
            return () => {
                counter++;
                return counter;
            };
        })();
        this.objects.forEach(gamedilObj => {
            gamedilObj.id = this._generateId();
        });
    }



    /**
     * @returns {boolean} is the game over?
     */
    over() {
        return this.round > 10;
    }

    load() {
        return load(this);
    }

    async start() {
        this.broadcastState();
        this.broadcastGameStart();
        while (!this.over()) {
            console.log("awaiting next round", __filename);
            await nextRound(this);
        }
        this.end();
    }

    broadcast(event, ...args){
        io.to(this.room).emit(event, ...args);
    }

    /**
     * ends the game
     */
    end() { 
        this.broadcastGameEnd();
        this.players.forEach( p => {
            const clone = p.gamedil.clone();
            p.gamedil = clone;
        });
    }

    /**
     * broadcasts the games state to the players by emitting gamedilState to game.room
     */
    broadcastState() {
        const state = currentState(this);
        this.broadcast(gamedilState, state);
    }

    broadcastLoadData(){
        // this.broadcast(gamedilLoadData, loadData(this));
        const data = loadData(this);
        this.players.forEach(player =>{
            player.socket.emit(gamedilLoadData, data, player.gamedil.id);
        });
    }

    broadcastLoadStatusInfo(msg){
        this.broadcast(gamedilLoadStatusInfo, msg);
    }

    broadcastGameStart(){
        this.broadcast(gamedilStartGame);
    }

    broadcastGameEnd(){
        this.broadcast(gamedilEndGame);
    }

    broadcastTurnStart(player){
        player.socket.emit(gamedilYourTurnStarted);
    }

    broadcastTurnEnd(player){
        player.socket.emit(gamedilYourTurnEnded);
    }

    async shoot(player, power){
        await shoot(this, player, power);
    }
}

/**
 * create a new Game
 * @param {*} players an array of players
 * @param {*} room the room in which the game takes place
 * @param {String} mapSrc source of the map to be played on
 */
module.exports = (players, room, mapSrc) => new Game(players, room, mapSrc);