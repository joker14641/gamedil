const canMove = require("./can-move");
const { fallGamedilObject } = require("./fall");
const gamedilMoveInDirection = "gamedil-move-in-direction";
const [left, stand, right] = [-1, 0, 1];
const directions = [left, stand, right];

const maxStepHeight = 4;
//precompute stepdistances
const stepDist = Array(maxStepHeight + 1).fill(0).map((_, index) => Math.sqrt(1 + index * index));
const move = (game, player, direction, timeDelta) => {
    // const fallSpeed = 256;
    // let dist = fallSpeed * timeDelta;
    // while ( dist > 0 && canMove.down(game, player.gamedil)) {
    //     player.gamedil.position.y++;
    //     dist--;
    // }
    const pGamedilObj = player.gamedil;
    const position = pGamedilObj.position;
    timeDelta = fallGamedilObject(game, pGamedilObj, timeDelta);
    if (direction === stand || timeDelta <= 0) {
        return;
    } else {
        const dir = direction === left ? "left" : "right";
        const moveSpeed = 256/1000;

        let dist = moveSpeed * timeDelta;
        while (dist > 0) {
            let currentStepHeight = 0;
            if (canMove[dir](game, pGamedilObj)) {
                position.x += direction;
                while (canMove.down(game, pGamedilObj) && currentStepHeight <= maxStepHeight) {
                    position.y++;
                    currentStepHeight++;
                }
                dist -= stepDist[currentStepHeight];
            } else {
                currentStepHeight = 1;
                position.y--;
                let moved = false;

                while (canMove.up(game, pGamedilObj) && currentStepHeight <= maxStepHeight) {
                    if (canMove[dir](game, pGamedilObj)) {
                        position.x += direction;
                        dist -= stepDist[currentStepHeight];
                        moved = true;
                        break;
                    }
                    position.y--;
                    currentStepHeight++;
                }
                if (!moved) {
                    position.y-= currentStepHeight;
                    return;
                }
            }
        }
    }

};

class MoveLoop {
    constructor(game, player) {
        this._game = game;
        this._player = player;
    }

    start() {
        const game = this._game;
        const player = this._player;
        let moveInDirection = stand;
        this._changeDirection = (direction) => {
            if (!directions.includes(direction)) {
                return;
            }
            moveInDirection = direction;
        };
        player.socket.on(gamedilMoveInDirection, this._changeDirection);
        let now, then;
        const idealTimeBetweenTicks = 1000 / 60;
        const moveLoop = () => {
            now = Date.now();
            move(game, player, moveInDirection, now - then);
            game.broadcastState();
            then = now;
            this._moveLoopTimeout = setTimeout(moveLoop, idealTimeBetweenTicks);
        };
        then = Date.now();
        moveLoop();
    }

    stop() {
        clearTimeout(this._moveLoopTimeout);
        this._player.socket.off(gamedilMoveInDirection, this._changeDirection);
    }
}

module.exports = (game, player) => new MoveLoop(game, player);

// //alternative looping approach
// const aBitFasterThanIdeal = 16;
// const moveLoop = () => {
//      now = Date.now();
//      if (then + idealTimeBetweenTicks < now) {
//           move(now - then);
//           then = now;
//      }
//      if (Date.now() + aBitFasterThanIdeal < then + idealTimeBetweenTicks) {
//          moveLoopTimeout= setTimeout(moveLoop);
//      } else {
//           moveLoopImmediate= setImmediate(moveLoop);
//      }
// };