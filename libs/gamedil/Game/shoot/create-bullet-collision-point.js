const collisionepsilon = 0.1;

class BulletCollisionPoint {
    constructor(bullet, collisionPointLocation) {
        this.game = bullet.game;
        this.bullet = bullet;
        this.relativePosition = { ...collisionPointLocation.position };
        this.normal = { ...collisionPointLocation.normal };
        this.motionData = {
            position: {
                x: bullet.motionData.position.x + this.relativePosition.x,
                y: bullet.motionData.position.y + this.relativePosition.y,
                mapLayer: 0
            },
            velocity: { ...bullet.motionData.velocity },
            acceleration: { ...bullet.motionData.acceleration }
        }
        this.timeToNextCheck = 0;
    }

    checkForCollisions() {
        let collisionFound = false;
        for (const collisionObj of this.bullet.canCollideWith) {
            const collisions = collisionObj.collisionsWith(this.motionData.position);
            if (collisions.length > 0) {
                const { position, velocity, acceleration } = this.motionData;
                collisions.map(c => {
                    c.collisionPoint = this;
                    c.motionData = {
                        position: { ...position },
                        velocity: { ...velocity },
                        acceleration: { ...acceleration }
                    };
                });
                this.bullet.registerCollisions(collisions);
                collisionFound = true;
            }
        }
        if (collisionFound) {
            return true;
        }

        this._prepareNextCheck();
        return false;
    }

    _prepareNextCheck() {

        this.timeToNextCheck = Math.min(...this.bullet.canCollideWith.map(collisionObj =>
            collisionObj.timeWithNoCollisions(this.motionData)));
        this.motionData = this.bullet
            .getFutureMotionData(this.motionData, this.timeToNextCheck);
    }

    recalculate() {
        const motionData = this.bullet.motionData;
        this.motionData = {
            position: {
                x: motionData.position.x + this.relativePosition.x,
                y: motionData.position.y + this.relativePosition.y,
                mapLayer: 0
            },
            velocity: { ...motionData.velocity },
            acceleration: { ...motionData.acceleration}
        }
        this.timeToNextCheck = 0;
    }
}

module.exports = (bullet, collisionPointLocation) =>
    new BulletCollisionPoint(bullet, collisionPointLocation);