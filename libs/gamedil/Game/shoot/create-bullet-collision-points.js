const createBulletCollisionPoint = require("./create-bullet-collision-point");

const byTimeToNextCheck = (bcp1, bcp2) => bcp1.timeToNextCheck - bcp2.timeToNextCheck;

class BulletCollisionPoints {
    constructor(bullet, collisionPointsPattern) {
        this._sortedArray = [...collisionPointsPattern]
            .map(cPLocation => createBulletCollisionPoint(bullet, cPLocation))
            .sort(byTimeToNextCheck);
    }

    /**
     * advances all collisionpoints for up to to ms or until one collides
     * @param {number} t time in ms
     * @returns {number} 0 if no collision occured or the difference between t and the time until the first collision
     */
    update(t) {
        const collisionPoints = this._sortedArray;
        const cp0 = collisionPoints[0];
        const t0 = cp0.timeToNextCheck;
        if (t0 > t) {
            collisionPoints.forEach(cp => {
                cp.timeToNextCheck -= t;
            });
            return 0;
        }

        if (cp0.checkForCollisions()) {
            return t - t0;
        }
        collisionPoints.slice(1).forEach(cp => {
            cp.timeToNextCheck -= t0;
        });
        collisionPoints.sort(byTimeToNextCheck);
        return this.update(t - t0);
    }

    recalculate(){
        this._sortedArray.forEach(collisionPoint => {collisionPoint.recalculate()});
        this._sortedArray.sort(byTimeToNextCheck);
    }
}

module.exports = (bullet, collisionPointsPattern) =>
    new BulletCollisionPoints(bullet, collisionPointsPattern);
