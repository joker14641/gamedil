const createBulletCollisionPoints = require("./create-bullet-collision-points");

// const supportedWeapons = ["grenade", "cannon"];
const collisionepsilon = 0.1;

const getIntegrator = (map, type) => {
    const integrateMotionData = (motionData, time) => {
        const {
            position: { x, y, mapLayer },
            velocity: { vx, vy },
            acceleration: { ax, ay }
        } = motionData;
        return {
            position: {
                x: x + time * (0.5 * ax * time + vx),
                y: y + time * (0.5 * ay * time + vy),
                mapLayer
            },
            velocity: {
                vx: vx + time * ax,
                vy: vy + time * ay
            },
            acceleration: { ax, ay }
        };
    };
    if (["cannon"].includes(type)) {
        return (motionData, time) => {
            const newMotionData = integrateMotionData(motionData, time);
            if ("rotation" in motionData) {
                const { vx, vy } = newMotionData.velocity;
                newMotionData.rotation = {
                    angleInDeg: Math.atan2(vy, vx) * 180 / Math.PI
                };
            }
            return newMotionData;
        };
    }
    if (["grenade"].includes(type)) {
        return (motionData, time) => {
            const newMotionData = integrateMotionData(motionData, time);
            if ("rotation" in motionData) {
                const { angularVelocity, angleInDeg } = motionData.rotation;
                newMotionData.rotation = {
                    angularVelocity,
                    angleInDeg: (angleInDeg - angularVelocity * time * 180 / Math.PI) % 360
                };
            }
            return newMotionData;
        };
    }
}

const getCollisionResolver = (bullet) => {
    const mapBoundaryCollision = "mapBoundaryCollision";
    const mapCollision = "mapCollision";
    const playerCollision = "playerCollision";
    const { type } = bullet;
    switch (type) {
        case "cannon":
            return () => {
                const collision0 = bullet.collisions[0];
                switch (collision0.type) {
                    case mapBoundaryCollision:
                        bullet.shotIsComplete = true;
                        break;
                    case playerCollision:
                    case mapCollision:
                        bullet.explode();
                        bullet.shotIsComplete = true;
                        break;
                    default:
                        break;
                }
                bullet.collisions = [];
            };
        case "grenade":
            return () => {
                const collision0 = bullet.collisions[0];
                switch (collision0.type) {
                    case mapBoundaryCollision:
                        bullet.shotIsComplete = true;
                        break;
                    case mapCollision:
                    case playerCollision:
                        const bcp = collision0.collisionPoint;
                        const { x: relX, y: relY } = bcp.relativePosition;
                        const {
                            position: {
                                x: collisionX,
                                y: collisionY
                            },
                            velocity: { vx, vy },
                            acceleration
                        } = collision0.motionData;
                        const { nx, ny } = bcp.normal;
                        const { corx, cory, moi, radius } = bullet;
                        const { angularVelocity: angv, angleInDeg } = bullet.motionData.rotation;

                        //move bullet to the collision, but a little bit off so that it doesnt collide anymore

                        const newX = collisionX - relX + 2 * collisionepsilon * nx;
                        const newY = collisionY - relY + 2 * collisionepsilon * ny;

                        //speedcomponents parallel resp. normal to tangential at collisionpoint before collision
                        var vp = vx * ny - vy * nx;
                        var vn = vx * nx + vy * ny;

                        //speedcomponents after collision
                        var vpa = ((1 - moi * corx) * vp + moi * (1 + corx) * radius * angv) / (1 + moi);
                        var vna = -cory * vn;

                        //updated angular velocity
                        const newAngv = ((1 + corx) * vp + moi * (1 + corx) * radius * angv) / (radius * (1 + moi));

                        //updated bullet speed
                        const newVx = vpa * ny + vna * nx;
                        const newVy = -vpa * nx + vna * ny;

                        bullet.motionData = {
                            position: {
                                x: newX,
                                y: newY,
                                mapLayer: 0
                            },
                            velocity: {
                                vx: newVx,
                                vy: newVy
                            },
                            acceleration,
                            rotation: {
                                angularVelocity: newAngv,
                                angleInDeg
                            }
                        };
                        bullet.collisionPoints.recalculate();
                        break;
                    default:
                        break;
                }
                bullet.collisions = [];
            };
    }
}

class Bullet {
    constructor(game, player, power, canCollideWith) {
        this.game = game;
        this.player = player;
        this.canCollideWith = canCollideWith;
        const gamedilPlayer = player.gamedil;
        const supportedWeapons = game.supportedWeapons;
        this.type = supportedWeapons.includes(gamedilPlayer.weapon) ?
            gamedilPlayer.weapon : supportedWeapons[0];

        const { x, y } = player.gamedil.position;
        const defaultSpeed = 256 / 1000;
        const angle = gamedilPlayer.aimAngle;
        this.motionData = {
            position: {
                x: x + gamedilPlayer.width * 0.5,
                y: y + gamedilPlayer.height * 0.5,
                mapLayer: 0
            },
            velocity: {
                vx: Math.cos(angle) * power * defaultSpeed,
                vy: Math.sin(angle) * power * defaultSpeed
            },
            acceleration: {
                ax: game.map.wind,
                ay: game.map.gravity
            },
            rotation: {
                angularVelocity: 0,
                angleInDeg: 0
            }
        };
        this.orientationAtRelease = this.motionData.velocity.vx > 0 ? 1 : -1;

        // this.angle = angle;

        this.corx = 0.015; //friction, usually between -1 and 1, the lower the more the glitchier
        this.cory = 0.7;  //bounciness, usually between 0 and 1, the higher the bouncier
        this.moi = 0.4; //moment of inertia (0.4 corresponds to a solid sphere, 2.0/3 is a hollow sphere)

        const initializationData = game.bullets.get(this.type);
        this.radius = initializationData.radius;
        this.collisionPoints = createBulletCollisionPoints(this,
            initializationData.collisionPointsPattern);
        this.shotIsComplete = false;
        if (this.type === "grenade") {
            this.shotTimeout = 5000;
        }
        else {
            this.shotTimeout = Infinity;
        }

        this.getFutureMotionData = getIntegrator(game.map, this.type);
        this.collisions = [];
        this.resolveCollisions = getCollisionResolver(this);
    }

    get state() {
        return {
            type: this.type,
            position: {
                x: this.motionData.position.x,
                y: this.motionData.position.y
            },
            angleInDeg: this.motionData.rotation.angleInDeg,
            orientation: this.orientationAtRelease
        };
    }
    async fire() {
        this.game.bulletsInAir.push(this);
        let then = Date.now();
        const idealTimeBetweenTicks = 1000 / 60;
        while (!this.shotIsComplete) {
            await new Promise((resolve) => {
                setTimeout(resolve, idealTimeBetweenTicks);
            });
            const now = Date.now();
            let timeLeft = now - then;
            while (timeLeft > 0) {
                const timeLeftAfterUpdate = this.collisionPoints.update(timeLeft);
                const updateTime = timeLeft - timeLeftAfterUpdate;
                this.shotTimeout -= updateTime;
                if (this.shotTimeout > 0) {
                    this.motionData = this.getFutureMotionData(this.motionData, updateTime);
                    timeLeft = timeLeftAfterUpdate;
                    if (this.collisions.length > 0) {
                        this.resolveCollisions();
                    }
                } else {
                    this.shotIsComplete = true;
                    this.explode();
                    break;
                }
            }
            this.game.broadcastState();
            then = now;
        }
        const i = this.game.bulletsInAir.indexOf(this);
        this.game.bulletsInAir.splice(i, 1);
    }

    registerCollisions(collisions) {
        this.collisions = [...this.collisions, ...collisions];
    }

    explode() {
        const { x, y } = this.motionData.position;
        const explosionRadius = 2 * this.radius;
        const map = this.game.map;

        map.explodeBulletAt(x, y, explosionRadius);
    };
}

module.exports = (game, player, power, canCollideWith) => new Bullet(game, player, power, canCollideWith);