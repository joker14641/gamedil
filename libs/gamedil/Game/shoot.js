const createBullet = require("./shoot/create-bullet");

module.exports = async (game, player, power) => {
    const gamedilObjects = game.objects.filter( o => o !==player.gamedil);
    const bullet = createBullet(game, player, power, [game.map, ...gamedilObjects]);
    await bullet.fire();
}