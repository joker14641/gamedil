const loadMap = require("../load-map");
// const loadBullets = require("./load-bullets");

const gamedilLoaded = "gamedil-loaded";
const gamedilFailedToLoad = "gamedil-failed-to-load";
const gamedilAssignOwnMatchId = "gamedil-assign-own-match-id";

module.exports = async (game) => {
    const { players } = game;
    game.broadcastLoadStatusInfo("Loading Game.");
    game.broadcastLoadData();
    const playerLoadPromises = [];
    players.forEach(function (p) {
        playerLoadPromises.push(new Promise((resolve, reject) => {
            function cleanUp() {
                p.socket.off(gamedilLoaded, p.gamedil.onLoad);
                p.socket.off(gamedilFailedToLoad, p.gamedil.onLoadError);
            }
            p.gamedil.onLoad = () => {
                if (!p.gamedil.loaded) {
                    p.gamedil.loaded = true;
                    // chat.sendMessage(p.id, "Gamedil: Loaded. Waiting for other player.");
                    cleanUp();
                    resolve();
                }
            };
            p.socket.on(gamedilLoaded, p.gamedil.onLoad);
            p.gamedil.onLoadError = () => {
                cleanUp();
                reject();
            };
            p.socket.on(gamedilFailedToLoad, p.gamedil.onLoadError);
        }));
    });

    const loadPlayers = async function () {
        await Promise.all(playerLoadPromises);
        game.broadcastLoadStatusInfo("Players loaded.");
    };
    const loadedMap = async function () {
        game.map = await loadMap(game.mapSrc);
        game.broadcastLoadStatusInfo("Map loaded.");
    };
    // const loadedBullets = async function (){
    //     await loadBullets(game.bullets);
    //     game.broadcastLoadStatusInfo("Bullets loaded.");
    // };
    await Promise.all([loadedMap(), loadPlayers()]);
    game.broadcastLoadStatusInfo("Players and server finished loading.");

    //assign random delays for initial turnorder
    players.forEach(p => {
        p.gamedil.delay = Math.random();
    });

    //assign spawn points to players
    players.forEach((p, i) => {
        p.gamedil.position.set(game.map.width * (i + 1) / 3, 0);
    });

    class BulletCollisionPointLocation {
        constructor(radius, angle) {
            const s = Math.sin(angle);
            const c = Math.cos(angle);
            this.position = {
                x: c * radius,
                y: s * radius
            }
            this.normal = {
                nx: -c,
                ny: -s
            }
        }
    }
    game.bullets.forEach((data, type) => {
        const radius = 0.5 * data.size;
        const nrOfCollisionPoints = 16;
        const alpha = 2 * Math.PI / nrOfCollisionPoints;
        data.radius = radius;
        data.collisionPointsPattern = Array(nrOfCollisionPoints).fill(0)
            .map((_, i) =>  new BulletCollisionPointLocation(radius, i * alpha));
    });

    game.broadcast(gamedilLoaded);
    // players.forEach(p => {
    //     p.socket.emit(gamedilAssignOwnMatchId, p.gamedil.id);
    // });
};