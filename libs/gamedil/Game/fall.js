const canMove = require("./can-move");


const fallGamedilObject = (game, gamedilObj, timeDelta) => {
    if(game.map.isOutOfBoundary(gamedilObj)){
        return;
    }
    const fallSpeed = 256 / 1000;
    let dist = fallSpeed * timeDelta;
    let fallDist = 0;
    while (dist > 0 && canMove.down(game, gamedilObj)) {
        gamedilObj.position.y++;
        dist--;
        fallDist++;
    }
    return timeDelta - fallDist / fallSpeed;
    // return {
    //     inAir: canMove.down(game, gamedilObj),
    //     timeLeft: timeDelta - fallDist / fallSpeed
    // };
};
exports.fallGamedilObject = fallGamedilObject;

const fallAllGamedilObjects = (game, timeDelta) => {
    // let allGrounded = true;
    for (const gamedilObj of game.objects) {
        fallGamedilObject(game, gamedilObj, timeDelta);
        // allGrounded = (!canMove.down(game, gamedilObj)) && allGrounded;
    }
    // return allGrounded;
};

/**
 * returns a promise that resolves when all gamedilObjects of game stand on solid ground
 * @param {Game} game
 */
exports.groundAllGamedilObjects = async (game) => {
    const isGrounded = (gamedilObj) =>  !canMove.down(game, gamedilObj); 
    const allGrounded = () => {
        for (const gamedilObj of game.objects) {
            if (!game.map.isOutOfBoundary(gamedilObj) && !isGrounded(gamedilObj)) {
                return false;
            }
        }
        return true;
    };
    const allGroundedPromise = new Promise((resolve) => {
        let now, then;
        const idealTimeBetweenTicks = 1000 / 60;
        const fallLoop = () => {
            now = Date.now();
            fallAllGamedilObjects(game, now - then);

            game.broadcastState();
            then = now;
            if (allGrounded()) {
                resolve();
            } else {
                setTimeout(fallLoop, idealTimeBetweenTicks);
            }
        };
        then = Date.now();
        if (allGrounded()) {
            resolve();
        } else {
            fallLoop();
        }
    });
    await allGroundedPromise;
};
