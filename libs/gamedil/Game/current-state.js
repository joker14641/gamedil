module.exports = (game) => ({
    mapChanges: game.map.state,
    objects: game.objects.map(gamedilObj => gamedilObj.state),
    bulletsInAir: game.bulletsInAir.map(bullet => bullet.state)
});