/**
 * is there a collision between the map and any collisionPoint relative to position x,y?
 * @param {*} map 
 * @param {*} x 
 * @param {*} y 
 * @param {*} collisionPoints 
 */
const collides = (map, x, y, collisionPoints) => {
    for (const collisionPoint of collisionPoints) {
        if (!map.isAir(x + collisionPoint.x, y + collisionPoint.y)) {
            return false;
        }
    }
    return true;
};

/**
 * can the obj be moved 1 unit to the left?
 * @param {*} game 
 * @param {*} gamedilObj 
 */
exports.left = (game, gamedilObj) => {
    return collides(game.map, gamedilObj.position.x - 1, gamedilObj.position.y, gamedilObj.collisionPoints);
};

/**
 * can the obj be moved 1 unit to the right?
 * @param {*} game 
 * @param {*} gamedilObj 
 */
exports.right = (game, gamedilObj) => {
    return collides(game.map, gamedilObj.position.x + 1, gamedilObj.position.y, gamedilObj.collisionPoints);
};

/**
 * can the obj be moved 1 unit upwards?
 * @param {*} game 
 * @param {*} gamedilObj 
 */
exports.up = (game, gamedilObj) => {
    return collides(game.map, gamedilObj.position.x, gamedilObj.position.y - 1, gamedilObj.collisionPoints);
};

/**
 * can the obj be moved 1 unit downwards?
 * @param {*} game 
 * @param {*} gamedilObj 
 */
exports.down = (game, gamedilObj) => {
    return collides(game.map, gamedilObj.position.x, gamedilObj.position.y + 1, gamedilObj.collisionPoints);
};