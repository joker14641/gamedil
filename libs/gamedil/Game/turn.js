// const {io} = require("@root/models/server-constants");
//const chat = require("../chat");
const createMoveLoop = require("./create-move-loop");
const { groundAllGamedilObjects } = require("./fall");

const gamedilStartCharging = "gamedil-start-charging";
const gamedilStopCharging = "gamedil-stop-charging";
const gamedilAimangle = "gamedil-aimangle";

/**
 * returns a promise, that resolves when the player's turn is over
 * @param {*} game 
 * @param {*} player the player whose turn it is
 */
const turn = async (game, player) => {
     player.gamedil.hasTurn = true;
     await groundAllGamedilObjects(game);
     game.broadcastTurnStart(player);
     let charging = false;
     let startedChargingAt;
     const walkPromise = new Promise((resolve, reject) => {
          const moveLoop = createMoveLoop(game, player);
          const moveTimeout = setTimeout(() => {
               moveLoop.stop();
               cleanUp();
               resolve();
          }, 30000);
          const handleStartCharging = (weapon) => {
               if(game.supportedWeapons.includes(weapon)){
                    player.gamedil.weapon = weapon;
               }
               charging = true;
               startedChargingAt = Date.now();
               moveLoop.stop();
               cleanUp();
               resolve();
          };
          player.socket.on(gamedilStartCharging, handleStartCharging);
          const cleanUp = () => {
               clearTimeout(moveTimeout);
               player.socket.off(gamedilStartCharging, handleStartCharging);
          };
          then = Date.now();
          moveLoop.start();
     });
     // chat.sendMessage(game.room, player.name +"'s turn.");
     // console.log(player.name);
     await walkPromise;
     if (charging) {
          const setAimAngle = (angle) => {
               if (!isNaN(angle)) {
                    player.gamedil.aimAngle = angle;
               }
          };
          player.socket.on(gamedilAimangle, setAimAngle);
          const chargePromise = new Promise((resolve) => {
               const chargeTimeout = setTimeout(() => {
                    cleanUp();
                    resolve(1);
               }, 3000);
               const handleStopCharging = () => {
                    const power = (Date.now() - startedChargingAt) / 3000;
                    cleanUp();
                    resolve(Math.min(power, 1));
               };
               player.socket.on(gamedilStopCharging, handleStopCharging);
               const cleanUp = () => {
                    clearTimeout(chargeTimeout);
                    player.socket.off(gamedilStopCharging, handleStopCharging);
               };
          });
          const power = await chargePromise;
          player.socket.off(gamedilAimangle, setAimAngle);
          await game.shoot(player, power);
     }
     game.broadcastTurnEnd(player);
     player.gamedil.hasTurn = false;
};

module.exports = turn;