// const { io } = require("@root/models/server-constants");
const chat = require("../chat");
// const loadMap = require("./load-map");
// const gamedilLoaded = "gamedil-loaded";
const gamedilFailedToLoad = "gamedil-failed-to-load";
// const gamedilAssignOwnMatchId = "gamedil-assign-own-match-id";
const createGame = require("./Game/create-game");

/**
 * waits for players to load and for game to be initialized, then starts the game
 * @param {} player1 
 * @param {} player2 
 */
module.exports = async function startGame(player1, player2) {
    const players = [player1, player2];
    const room = chat.createRoom("Gamedil", player1, player2);
    const mapSrc = "map4.png";
    const game = createGame(players, room, mapSrc);
    // players.forEach(function (p) {
    //     p.gamedil.inGame = true;
    // });
    // chat.sendMessage(room, "Gamedil: Loading Game.");
    // io.to(room).emit("load-gamedil", {
    //     map: "map4.png",
    //     players: ["hero.png", "hero1.png"]
    // });
    // game.broadcastLoadData();
    // const playerLoadPromises = [];
    // players.forEach(function (p) {
    //     playerLoadPromises.push(new Promise((resolve, reject) => {
    //         function cleanUp() {
    //             p.socket.off(gamedilLoaded, p.gamedil.onLoad);
    //             p.socket.off(gamedilFailedToLoad, p.gamedil.onLoadError);
    //         }
    //         p.gamedil.onLoad = () => {
    //             if (!p.gamedil.loaded) {
    //                 p.gamedil.loaded = true;
    //                 chat.sendMessage(p.id, "Gamedil: Loaded. Waiting for other player.");
    //                 cleanUp();
    //                 resolve();
    //             }
    //         };
    //         p.socket.on(gamedilLoaded, p.gamedil.onLoad);
    //         p.gamedil.onLoadError = () => {
    //             cleanUp();
    //             reject();
    //         };
    //         p.socket.on(gamedilFailedToLoad, p.gamedil.onLoadError);
    //     }));
    // });
    try {
        // const loadPlayers = async function () {
        //     await Promise.all(playerLoadPromises);
        //     chat.sendMessage(room, "Gamedil: Players loaded.");
        // };
        // const loadedMap = async function () {
        //     game.map = await loadMap("public/graphics/map4.png");
        //     chat.sendMessage(room, "Gamedil: Map loaded. ");
        // };
        // await Promise.all([loadedMap(), loadPlayers()]);
        // chat.sendMessage(room, "Players and map loaded.");

        // //assign random delays for initial turnorder
        // players.forEach(p => {
        //     p.gamedil.delay = Math.random();
        // });

        // //assign spawn points to players
        // players.forEach((p, i) => {
        //     p.gamedil.position.set(game.map.width * (i + 1) / 3, 0);
        // });

        // io.to(room).emit(gamedilLoaded);
        // players.forEach(p => {
        //     p.socket.emit(gamedilAssignOwnMatchId, p.gamedil.id);
        // });
        // chat.sendMessage(room, "Starting game");
        // game.start();

        await game.load();
        game.start();
    } catch (error) {
        console.log(error, __filename);
        game.broadcast(gamedilFailedToLoad);
        players.forEach((p) => { p.gamedil.inGame = false; });
    }
};
